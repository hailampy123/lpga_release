# LPGA: Line-Of-Sight Parsing with Graph-based Attention for Math Formula Recognition

*Copyright (c) 2017-2019 Mahshad Mahdavi, Michael Condon, Kenny Davila, and Richard Zanibbi*  
*Document and Pattern Recognition Lab, Rochester Institute of Technology, USA*  
<https://www.cs.rit.edu/~dprl>

*LgEval and CROHMELib: Copyright (c) 2013-2019 Richard Zanibbi and Harold Mouchere*

## Installation

**Dependencies**: Python3. For the complete list of dependencies check out requirements.txt 

	pip3 install -r requirements.txt 
	
**Operating System**: While this code should work on Windows systems as well, we have only tested on Linux and MacOS X so far.

From the main directory, issue ``make``, and follow the instructions. See the script file 'example' for details of how these example results are produced. This simple examples recognized just 10 infty images.

<!---
- Python3
- intervaltree
- cython (version 0.25.2 )  to install, use:
        'pip install cython==0.25.2'
- scikit-learn (v 0.17.1 or later ... maybe).

doxygen  (for documentation - not a python package!)
--->

## infty Data Set

We use InftyMCCDB-2, a modified version of InftyCDB-2  which contains mathematical expressions from scanned article pages.

The dataset has 21,056 math expressions. We remove formulas with matrices and grids, leaving 19,381 formulas.

The ground truth created by Michael Condon for the infty data set is located in the Data/ directory. The set of .lg (label graph) ground truth files are provided, along with a .png image for each expression. Download the dataset from here [InftyMCCDB-2](https://zenodo.org/record/3483048#.XaCwmOdKjVo).

## Getting Started

1. Install the [InftyMCCDB-2](https://zenodo.org/record/3483048#.XaCwmOdKjVo) dataset and unzip the files into Data/Expressions to make infty data available for use.

2. To run the pre-trained system for
   the infty data set with LPGA-RF, use:

    	python3 RF_full_recognition_script.py configs/full_system_infty.conf 1 1 1

    The '1 1 1' indicates to load pre-trained classifiers, segmenters,
    and parsers.
    
3. To run the pre-trained system for
   the infty data set with LPGA-CNN, use: 
   
   		python3 CNN_full_recognition_test.py configs/CNNs/full_system_infty.conf 1 1 1
  
4. To generate html and LaTeX documentation in the src/doc directory, using doxygen:

        cd src/doc/
        make

     This automatically extracts the call structure and function
    signatures in the source files.

    This requires the 'doxygen' package to be installed and working, which is widely available.


## Evaluation Scripts

LgEval and CROHMELib are provided with this package. To install these, go into their subdirectories under the 'evaluation' directory, and follow the installation instructions provided there. 


Notes on Usage and Parameters
-------------------------------------------

These comments pertain to the "src/" directory.

#### SCRIPTS:

**Usage:**`python3 RF_full_recognition_script.py <Path to the config file> <Load Segmenter> <Load Symbol Classifier> <Load Parser>`

**Example:**`python3 RF_full_recognition_script.py configs/full_recognition_script.conf 1 1 1    # Run *without* training`

 ``<Configuration file>`` includes basic parameters for the entire system, including the location of sub configuration files for the segmenter, symbol classifier, and parser.

``<Load Parameters>`` are boolean values (1 or 0) that indicate if the given expert should be loaded from a
            saved expert file. If the expert is not loaded from a saved source it will be retrained and saved.

#### CONFIG PARAMETERS 

*  [Recognition system parameters](htmls/param1.html)

*  [Data parameters](htmls/param2.html)
        Used by both the system config, for loading test data, and the expert configs, for loading training datasets.

	
## LICENSE

GPL

## CONTACT

mxm7832@rit.edu

## CITATION

If you found this code/paper to be useful in your research, please consider citing the following:

    @inproceedings{mahdavi2019lpga,
    title={LPGA: Line-Of-Sight Parsing with Graph-based Attention for Math Formula Recognition},
     author={Mahdavi, Mahshad and Condon, Michael and Davila, Kenny and Zanibbi, Richard},
    booktitle={Proceedings of the International Conference on Document Analysis and Recognition},
    year={2019}
    }

