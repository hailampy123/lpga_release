#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  2 17:30:06 2019

@author: mxm7832
"""



import matplotlib.pyplot as plt
import numpy as np
import sys
import os
import shutil
from recognizer.control.controller import Controller
from recognizer.configuration.configuration import Configuration
from recognizer.data.dataset_builder import TYPES_DATA_SET
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES
import time

def main():    
    config = Configuration.from_file(sys.argv[1])
    #configPath='configs/CNNs/full_system_infty.conf'
    #config = Configuration.from_file(configPath)
       
    subconfig_dir = config.get_str("SUB_CONFIGS_DIR")
    subconfigs_names = config.get("SUB_CONFIGS")

    control = Controller(config, "default")

    experts_names = config.get("CNN_EXPERT_LIST")
    expert_dir = config.get_str("CNN_EXPERT_DIR")

    ### Load Test Expressions ###
    print("Loading Test Expressions")
    control.loadExpressions(name="testing", isTraining=False)    
    control.preprocessExpressions("testing")
    control.createExpressionGraphs("testing")
   
###############################################################################################
    ### Do segmentation ###
###############################################################################################
    ### Do Segmentation ###
    t=time.time()
    print("Starting Segmentation")
    seg_config = Configuration.from_file("configs/sub_segmentation.conf")
    control.addConfig(seg_config, "seg_config")

    experts_names[0]="symbol_seg_v5_Geo_scf_3_6_p10_10_scf_3_10_p10_175" 
    print("Loading Segmenter")
    control.loadExpert( "../output/classifiers/symbol_seg_v5_Geo_scf_3_6_p10_10_scf_3_10_p10_175.dat", name=experts_names[0])

    # Get Test Segmentation Dataset
    print("Creating Test Segmentation Dataset")
    control.createDataset("testing", TYPES_DATA_SET.Segmentation, label_source=experts_names[0], dataset_name="seg_data_testing", config_name="seg_config")

    # Do Segmentation
    print("Doing Segmentation")
    control.useExpert("testing", experts_names[0], "seg_data_testing", True)

    # Clean After Segmentation
    del control.datasets["seg_data_testing"]

###############################################################################################
    ### Do classification###
############################################################################################### 
    print("Starting Symbol Classification")
    symRec_config = Configuration.from_file(subconfig_dir+subconfigs_names[1]+".conf")
    control.addConfig(symRec_config, "sym_config")
    
    #generate symbol graph based on segmentation results 
    control.createExpressionGraphs("testing", "sym_config")
           
    #Merge 213 classes to 207
    control.merge_symbol_labels("testing")
    
    # Get the trained model 
    print("Loading Symbol Classifier")
    control.load_cnnExpert(expert_dir+experts_names[1]+".h5",task=TYPES_DATA_SET.Symbol.value, name=experts_names[1])
    
    #Making symbol images to fed into generator
    control.generateImage_CNN("sym_config",TYPES_DATA_SET.Symbol.value)#configname,task

    print("Classifying Symbols")
    control.symbolRec_CNN(experts_names[1],"sym_config",labels=True) #MM remove labels=False for run

   # Clean After symbol classification
    del control.CNNexperts[experts_names[1]]
    shutil.rmtree((control.configs["sym_config"].get_str('CNN_IMAGE_OUTPUT_DIR')))
             
###########################################################################################
    ### Do Parsing ###
########################################################################################### 

    print("Starting Parsing")
    par_config = Configuration.from_file(subconfig_dir+subconfigs_names[2]+".conf")
    control.addConfig(par_config, "par_config")
    
    print("Loading Parser")
    control.load_cnnExpert(expert_dir+experts_names[2]+".h5",task=TYPES_DATA_SET.Relation.value, name=experts_names[2])

   # Get Test Relation features
    print("Creating Test Parsing geometric Features")
    control.createDataset("testing", TYPES_DATA_SET.Relation, label_source=experts_names[2],
                                         dataset_name="par_data_testing", config_name="par_config")

    #Making pair images to fed into generator
    control.generateImage_CNN("par_config",TYPES_DATA_SET.Relation.value,
                              featureSet_name="par_data_testing")#configname,task, featureSet_name

    print("Parsing Expressions")    
    control.parse_CNN(experts_names[2], "par_config", "par_data_testing",labels=TYPES_DATA_SET.Relation.value)#labels could be None

   # Clean After Parsing
    del control.datasets["par_data_testing"]
    del control.CNNexperts[experts_names[2]]
    shutil.rmtree((control.configs["par_config"].get_str('CNN_IMAGE_OUTPUT_DIR')))

##############################################################################################
    #give nodes the GT labels 
    '''    
    print("Outputting Label Graph Files")
    expressions = control.getExpressions("testing")
    for exprId in expressions.keys():
        expr = expressions[exprId]
        for symbol in expr.expressionGraph.nodes():
            expr.expressionGraph.node[symbol][NODE_ATTRIBUTES.PREDICTED_CLASS] = expr.expressionGraph.node[symbol][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS]
        expressions[exprId] = expr
    control.expressionSets['testing'] = expressions
    '''
##############################################################################################    

   ### Output  Final results ###
    control.outputExpressions("testing", location=config.get_str("OUTPUT_DIR"), lg_type="OR")

    elapsed = time.time() - t
    print('time spent for full recognition:',elapsed)  
   
if __name__ == "__main__":
    main()














