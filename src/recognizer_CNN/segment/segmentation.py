#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan  5 15:32:42 2019

@author: mxm7832
"""
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES
import numpy as np

def assign_edge_prob(predict_probs,src,expressions):

    for i in range(len(src)):
        exprId, pid, cid = src[i]
        classes=predict_probs[exprId+'_'+pid+'_'+cid][1]
        probs=predict_probs[exprId+'_'+pid+'_'+cid][0]
        graph = expressions[exprId].expressionGraph
        graph.edge[pid][cid]['classes'] = classes
        graph.edge[pid][cid]['scores'] = probs


def segment_prob(expressions,src,pred_dict,convert=True):
    # Make highest score the weight of each edge and record its class
    assign_edge_prob(pred_dict,src,expressions)
    for exprId, expr in expressions.items():
        for pid,cid in expr.expressionGraph.edges():
            expr.expressionGraph.edge[pid][cid]["weight"] = expr.expressionGraph.edge[pid][cid]["scores"][0]
            expr.expressionGraph.edge[pid][cid][EDGE_ATTRIBUTES.PREDICTED_SEGMENTATION] = expr.expressionGraph.edge[pid][cid]['classes'][0]
    # Perform Edmonds' algorithm and convert result to symbol graph
    if convert:
        for exprId, expr in expressions.items():
            expr.convertSymbolLevel()
            expressions[exprId] = expr