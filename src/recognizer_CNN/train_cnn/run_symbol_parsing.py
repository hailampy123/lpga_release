#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec  2 23:54:42 2018

@author: mxm7832
"""


from cc_segmentation_train import Training
import os
os.environ["CUDA_VISIBLE_DEVICES"]="1"
from keras.models import load_model

def main(Params,targetPath,contextPath):
    #Training
    model,scores=fine_tune_2branch(Params,targetPath,contextPath)
    print(scores)
    model.save('../../../output/CNN_classifiers/'+Params['exp_name']+'.h5')
    
    #CROSS VALIDATION 
    #recognition_rates=cross_validation(Params,modelName,targetPath,contextPath)    
    #rates=multiple_run(Params,modelName,targetPath,contextPath)

def fine_tune_vgg16(params,targetPath):
    
    testImgPath=targetPath+'test/'
    trainImgPath= targetPath+'train/'
    train=Training(**params) 
    
    #visual features only
    model=train.fineTune_VGG16_model_vis(trainImgPath,testImgPath)
    
    #visual features+geo (tanh)
    #model=train.fineTune_VGG16_model(trainImgPath,testImgPath)
    
    scores=train.evaluate_model(model,trainImgPath)
    
    return model,scores

def fine_tune_2branch(params,targetPath,contextPath):  
    
    train=Training(**params)
    #define paths
    trainImgPath1= targetPath+'train/'
    trainImgPath2= contextPath +'train/'
    testImgPath1 = targetPath+'test/'
    testImgPath2 = contextPath +'test/'
    
    model=train.two_branch_model(trainImgPath1,trainImgPath2,testImgPath1,testImgPath2)
    scores=train.evaluate_model(model,testImgPath1,testImgPath2)

    #MM: giving training path to evaluation for validation sets!!!
    #scores=train.evaluate_model(model,trainImgPath1,trainImgPath2)
    return model,scores

def test_saved_modesl(modelPath):
    model=load_model(modelPath)
    my_testing_generator=data_gen_2D(testImgPath1,testImgPath2,train.test_labels,batch_size=64,
                                             dataset_name=train.dataset,feat=train.testFeatures)

    prediction=model.predict_generator(generator= my_testing_generator, steps=None,
                                           max_queue_size=10, workers=multiprocessing.cpu_count(),
                                           use_multiprocessing=True, verbose=0)


if __name__ == '__main__':
    
    Params = {'dim': (224,224,3),
      'b_size': 64, #32 for segmentation
      'n_classes': 9,
      'learning_rate': 0.00001,
      'datasetName': 'infty',
      'n_epochs': 70, 
      'testLabelPath':'cnn_data/parTarget/labels/test.txt',
      'trainLabelPath':'cnn_data/parTarget/labels/train.txt',
      'validLabelPath':'cnn_data/parTarget/labels/valid.txt',
      'exp_name': 'Parsing_latefusion_FC_finetune',
      'featurePath': 'cnn_data/spatialFeat/parsing/', #geofeat
      }  
    #Make sure you have generated labels and images before training using "infty_img_generation.py"
    targetPath='cnn_data/parTarget/'
    contextPath='cnn_data/parContext/'

    main(Params,targetPath,contextPath)
