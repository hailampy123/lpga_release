#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 18:52:39 2018

@author: mxm7832
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from skimage import io, measure
from Data_generator import data_gen,data_gen_2D,seg_gen_3D,relation_class_index
import multiprocessing
from keras.callbacks import ModelCheckpoint, TensorBoard, EarlyStopping 
from keras.applications.vgg16 import VGG16
from keras.layers import Dense, GlobalAveragePooling2D,Flatten,Dropout,concatenate,Input,Activation
from keras.models import Model
from keras.optimizers import SGD,RMSprop,Nadam
#from load_CROHME import CROHME_labels
from load_INFTY import infty_labels,loadFeat
from sklearn.metrics import confusion_matrix
#from symbol_classification_vgg16_RF import report_performance
from keras.utils import plot_model       
#from extract_CCpair_info import loadFeat

class Training():
    #TODO: make config files for different experiments to get parameters from
    def __init__(self, dim,b_size,n_classes,learning_rate,datasetName,n_epochs,
                 trainLabelPath,testLabelPath,validLabelPath,exp_name,featurePath=None):
        #define network parameters
        self.dim=dim
        self.b_size=b_size
        self.n_classes=n_classes
        self.learning_rate=learning_rate
        self.dataset= datasetName
        self.n_epochs=n_epochs
        self.experimentName=exp_name
        #define labels
        if self.dataset=='infty':
            train_labels,test_labels,valid_labels,funcs=infty_labels(trainLabelPath,testLabelPath,validLabelPath)
        elif self.dataset=='crohme':
            train_labels,test_labels,valid_labels,funcs=CROHME_labels(trainLabelPath,testLabelPath,validLabelPath)
        self.funcs=funcs
        self.transformers=funcs
        self.train_labels=train_labels
        self.test_labels=test_labels
        self.valid_labels=valid_labels
	# load geometric edge features
        self.testFeatures=None
        self.trainFeatures=None
        if featurePath:
            self.testFeatures=loadFeat(featurePath+'test/spatial_features.txt')
            self.trainFeatures=loadFeat(featurePath+'train/spatial_features.txt')
            
        self.predictions={}              
    def two_branch_model(self,trainImgPath1,trainImgPath2,testImgPath1,testImgPath2):

        #*************************** loading data ************************************************
        my_training_generator=data_gen_2D(trainImgPath1,trainImgPath2,self.train_labels,batch_size=self.b_size,
                                          dataset_name=self.dataset,feat=self.trainFeatures) 
        my_validation_generator=data_gen_2D(testImgPath1,testImgPath2,self.valid_labels,batch_size=64,
                                            dataset_name=self.dataset,feat=self.testFeatures) 
        
        #******************************* target branch ********************************************
        base_model1 = VGG16(weights='imagenet', include_top=False,input_shape =self.dim)
        x1 = base_model1.output
        x1 = GlobalAveragePooling2D()(x1) 
        #x1 = Dense(512, activation='relu')(x1)
        for layer in base_model1.layers:
            layer.trainable = False
        #******************************* context branch ********************************************    
        base_model2 = VGG16(weights='imagenet', include_top=False,input_shape = self.dim) 
        x2 = base_model2.output
        x2 = GlobalAveragePooling2D()(x2) 
        #x2 = Dense(512, activation='relu')(x2)         
        for layer in base_model2.layers:
            layer.name = layer.name + str("_2")
            layer.trainable = False
        #******************************************************************************************            
        x=concatenate([x1,x2])
        x = Dense(1024, activation='tanh')(x) #mm: changed to tanh
        x= Dropout(0.5)(x)

        #******************************* feature branch *******************************************
        features= Input(shape=(50,),name='Features') #MM: scale spatial features between[-1,1]
        normFeat=Activation('tanh')(features)

        merged= concatenate([x,normFeat])
        
        predictions = Dense(self.n_classes, activation='softmax')(merged) 
        
        #plot_model(model,to_file='demo.png',show_shapes=True)         
        model = Model(inputs=[base_model1.input,base_model2.input,features], outputs=predictions)
        #first freez everything and train few epochs the dense layers
        model.compile(optimizer='rmsprop', loss='categorical_crossentropy',metrics=['accuracy']) 
        #model.load_weights('../output/model_weights/segmentation/lateFusion_R1.75_us+context/weights-improvement-46-0.0058.hdf5', by_name=False) 
        
        model.fit_generator(generator= my_training_generator,epochs=3, workers=multiprocessing.cpu_count(),
                            use_multiprocessing=True,shuffle=True,class_weight=None)

        #******************************* fine tune ************************************************             
        #featLayers=['Features', 'activation_1']
           
        for layer in model.layers[:30]:
            layer.trainable = False
        for layer in model.layers[30:]:
            #if layer.name not in featLayers:
            if layer.name.startswith('Features') or layer.name.startswith('activation'):
                layer.trainable = False
            else:
                layer.trainable = True
                print(layer.name)
            
        nadam=Nadam(lr= self.learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004) # decay=0.004
        model.compile(optimizer=nadam, loss='categorical_crossentropy',metrics=['accuracy'])
    
        callbacks_list = self.defineCallbacks(['tensorboard','checkpoint','earlyStopping']) 
        
        model.fit_generator(generator=my_training_generator,epochs=self.n_epochs,initial_epoch=0,validation_data=my_validation_generator,
                                    workers=multiprocessing.cpu_count(), use_multiprocessing=True,
                                    callbacks=callbacks_list,shuffle=True,class_weight=None) 
        return model        

    def fineTune_VGG16_model(self,trainImgPath,testImgPath):
        
        #***********Loading Data**************************        
        my_training_generator=data_gen(trainImgPath,self.train_labels,batch_size=self.b_size,
                                       dataset_name=self.dataset,feat=self.trainFeatures) 
        my_validation_generator=data_gen(testImgPath,self.valid_labels,batch_size=64,dataset_name=self.dataset,
                                         feat=self.testFeatures)     
        
        #***************************************************
        base_model = VGG16(weights='imagenet', include_top=False,input_shape = self.dim)        
        # add a global spatial average pooling layer
        x = base_model.output
        x = GlobalAveragePooling2D()(x)
        x = Dense(1024, activation='tanh')(x)
        x= Dropout(0.5)(x) 
	#27 geometric features
        features= Input(shape=(50,),name='Features') #MM: scale spatial features between[-1,1]
        normFeat=Activation('tanh')(features)

        merged= concatenate([x,normFeat])
        predictions = Dense(self.n_classes, activation='softmax')(merged)
        
        # this is the model we will train
        model = Model(inputs=[base_model.input,features], outputs=predictions)
        #model = Model(inputs=base_model.input, outputs=predictions)
        
        for layer in base_model.layers:
            layer.trainable = False
            
        #rmsprop=RMSprop(lr=0.00001, rho=0.9, epsilon=None, decay=0.0)    
        model.compile(optimizer='rmsprop', loss='categorical_crossentropy',metrics=['accuracy']) 
        #model.load_weights('../output/model_weights/segmentation/earlyFusion_R1.75_us+context/weights-improvement-50-0.0137.hdf5', by_name=False)         
        model.fit_generator(generator= my_training_generator,epochs=3, workers=multiprocessing.cpu_count(),
                            use_multiprocessing=True,shuffle=True,class_weight=None)                                                
        #*********************************************************************************************
        #featLayers=['Features', 'activation_1']    
        for layer in model.layers[:15]:
            #print layer.name
            layer.trainable = False
        for layer in model.layers[15:]:
           # if layer.name not in featLayers:
            if layer.name.startswith('Features') or layer.name.startswith('activation'):
                layer.trainable = False
            else:
                layer.trainable = True
                print(layer.name)
                        
        nadam=Nadam(lr= self.learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004) # decay=0.004
        #sgd = SGD(lr=1e-3, decay=1e-6, momentum=0.9, nesterov=True) optimizer sgd?
        #rmsprop=RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)
        model.compile(optimizer=nadam, loss='categorical_crossentropy',metrics=['accuracy'])
    
        callbacks_list = self.defineCallbacks(['tensorboard','checkpoint','earlyStopping'])
                
        model.fit_generator(generator= my_training_generator,epochs=self.n_epochs,initial_epoch=0,validation_data=my_validation_generator,
                                    workers=multiprocessing.cpu_count(), use_multiprocessing=True,
                                    callbacks=callbacks_list,shuffle=True,class_weight=None) 
        return model        
    #**********************************************************************************************
    def evaluate_model(self,model,testImgPath1,testImgPath2=None,outDir='../output/outputLG/'):
        
        if testImgPath2:
            #MM: features are train features cause I am calculating the validation accuracy
            my_testing_generator=data_gen_2D(testImgPath1,testImgPath2,self.test_labels,batch_size=64,
                                             dataset_name=self.dataset,feat=self.testFeatures)         
        else:
            my_testing_generator=data_gen(testImgPath1,self.test_labels,batch_size=64,
                                          dataset_name=self.dataset,feat=self.testFeatures)

        scores=model.evaluate_generator(my_testing_generator, steps=None, max_queue_size=15, 
                                     workers=multiprocessing.cpu_count(),use_multiprocessing=True)
        ''' 
        print('Classifier Rate:', scores)
        
        prediction=model.predict_generator(generator= my_testing_generator, steps=None,
                                           max_queue_size=10, workers=multiprocessing.cpu_count(),
                                           use_multiprocessing=True, verbose=0)        
        
        lgPath=outDir+self.experimentName+'/'
        os.mkdir(lgPath)
        self.write_lg(np.argmax(prediction,axis=1),my_testing_generator.file_names,lgPath)
        print('lg files are written')
        '''
        return scores
    
    def write_lg(self,prediction,fileNames,lgPath):
        segInfo=self.readImgList(fileNames,prediction)
        for file in segInfo.keys():
            with open(lgPath+file+'.lg', 'w') as f:
                f.write('# IUD, '+file+'\n')
                f.write('# objects\n')                
                #copy the primitivelabels from gt
                for line in open('../Data/typeset/infty_segmentation/segGT/test/'+file+'.lg'):
                    if line.startswith('O'):
                        f.write(line)
                #write the seg relations
                f.write('# Relations\n')                
                pairlabels=segInfo[file] 
                for pair,label in pairlabels.items():
                    segLabel='SPLIT'          
                    if label==1:
                        segLabel='MERGE' 
                    line='R, '+pair[0]+', '+pair[1]+', '+segLabel+', 1.0'+'\n'
                    f.write(line)                    
            f.close()
                
 
    @staticmethod
    def readImgList(fileNames,prediction):
        reference={}
        fileIds={}
        for i,file in enumerate(fileNames):
            fileId=file.split('_')[0]
            parent=file.split('_')[1]
            child=file.split('_')[2]
            reference[file]=int(prediction[i])
            if fileId in fileIds.keys():
                fileIds[fileId].append((parent,child))
            else:
                fileIds[fileId]=[(parent,child)]
        info={}
        for filename in fileIds.keys():
            pairLabels={}
            for pair in fileIds[filename]: 
                pairLabels[pair]=reference[filename+'_'+pair[0]+"_"+pair[1]]
            info[filename]=pairLabels
            
        return info
        
    @staticmethod
    def write_predictionOutput(fileNames,prediction,outputPath):
        ref=relation_class_index()
        with open(outputPath, 'w') as f:        
            for i,file in enumerate(fileNames):
                fileId=file.split('_')[0]
                parent=file.split('_')[1]
                child=file.split('_')[2]
                pred=prediction[i,:]
                sort_pred=sorted(pred,reverse=True)
                class_idx=sorted(range(len(pred)), key=lambda k: -pred[k])
                class_names=[ref[str(i)] for i in class_idx]
                line=fileId+'_'+parent+'_'+child+'_'+str(sort_pred)+'_'+str(class_names)+'\n'
                f.write(line)
        f.close()
        
    def defineCallbacks(self,callback_types):
        callback=[]
        if 'checkpoint' in callback_types:
            weightPath='../output/model_weights/parsing/'+self.experimentName
            if not os.path.isdir(weightPath):
                os.mkdir(weightPath)
            filepath=weightPath+"/weights-improvement-{epoch:02d}-{loss:.4f}.hdf5"
            checkpoint = ModelCheckpoint(filepath, monitor='loss', verbose=1, save_best_only=True, mode='min')
            callback.append(checkpoint)

        if 'earlyStopping' in callback_types:    
            earlystopping= EarlyStopping(monitor='val_loss',patience=8)
            callback.append(earlystopping)

        if 'tensorboard' in callback_types:
            logPath="../output/logs/par_logs/"+self.experimentName
            if not os.path.isdir(logPath):
                os.mkdir(logPath)
            tensorboard = TensorBoard(log_dir=logPath)
            callback.append(tensorboard)

        return callback
    
    def three_branch_model(self,trainCC,trainContext,testCC,testContext):

        #*************************** loading data ************************************************
        my_training_generator=seg_gen_3D(trainCC,trainContext,self.train_labels,batch_size=self.b_size,
                                          dataset_name=self.dataset,feat=self.trainFeatures) 
        my_validation_generator=seg_gen_3D(testCC,testContext,self.valid_labels,batch_size=64,
                                            dataset_name=self.dataset,feat=self.testFeatures) 
        
        #******************************* parent branch ********************************************
        base_model1 = VGG16(weights='imagenet', include_top=False,input_shape =self.dim)
        x1 = base_model1.output
        x1 = GlobalAveragePooling2D()(x1) 
        #x1 = Dense(512, activation='relu')(x1)
        for layer in base_model1.layers:
            layer.trainable = False
        #******************************* child branch ********************************************    
        base_model2 = VGG16(weights='imagenet', include_top=False,input_shape = self.dim) 
        x2 = base_model2.output
        x2 = GlobalAveragePooling2D()(x2) 
        #x2 = Dense(512, activation='relu')(x2)         
        for layer in base_model2.layers:
            layer.name = layer.name + str("_2")
            layer.trainable = False
        #******************************* context branch ********************************************    
        base_model3 = VGG16(weights='imagenet', include_top=False,input_shape = self.dim) 
        x3 = base_model3.output
        x3 = GlobalAveragePooling2D()(x3) 
        #x2 = Dense(512, activation='relu')(x2)         
        for layer in base_model3.layers:
            layer.name = layer.name + str("_3")
            layer.trainable = False
        #******************************************************************************************            
        x=concatenate([x1,x2,x3])
        x = Dense(1024, activation='tanh')(x) #mm: changed to tanh
        x= Dropout(0.5)(x)

        #******************************* feature branch *******************************************
        features= Input(shape=(27,),name='Features') #MM: scale spatial features between[-1,1]
        normFeat=Activation('tanh')(features)

        merged= concatenate([x,normFeat])
        
        predictions = Dense(self.n_classes, activation='softmax')(merged) 
        
        #plot_model(model,to_file='demo.png',show_shapes=True)         
        model = Model(inputs=[base_model1.input,base_model2.input,base_model3.input,features], outputs=predictions)
        #first freez everything and train few epochs the dense layers
        model.compile(optimizer='rmsprop', loss='categorical_crossentropy',metrics=['accuracy']) 
        #model.load_weights('../output/model_weights/segmentation/lateFusion_R1.75_us+context/weights-improvement-46-0.0058.hdf5', by_name=False) 
        
        model.fit_generator(generator= my_training_generator,epochs=3, workers=multiprocessing.cpu_count(),
                            use_multiprocessing=True,shuffle=True,class_weight=None)
        
        #******************************* fine tune ************************************************             
        featLayers=['Features', 'activation_1']
           
        for layer in model.layers[:45]:
            layer.trainable = False
        for layer in model.layers[45:]:
            if layer.name not in featLayers:
                layer.trainable = True
                print(layer.name)
            
        nadam=Nadam(lr= self.learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004) # decay=0.004
        model.compile(optimizer=nadam, loss='categorical_crossentropy',metrics=['accuracy'])
    
        callbacks_list = self.defineCallbacks(['checkpoint','tensorboard']) 
        
        model.fit_generator(generator=my_training_generator,epochs=self.n_epochs,initial_epoch=0,validation_data=my_validation_generator,
                                    workers=multiprocessing.cpu_count(), use_multiprocessing=True,
                                    callbacks=callbacks_list,shuffle=True,class_weight=None) 
        return model        
##########################################################################################3
        #visual features only, no geometric layer
    def fineTune_VGG16_model_vis(self,trainImgPath,testImgPath):
        
        #***********Loading Data**************************        
        my_training_generator=data_gen(trainImgPath,self.train_labels,batch_size=self.b_size,
                                       dataset_name=self.dataset,feat=self.trainFeatures) 
        my_validation_generator=data_gen(testImgPath,self.valid_labels,batch_size=64,dataset_name=self.dataset,
                                         feat=self.testFeatures)     
        
        #***************************************************
        base_model = VGG16(weights='imagenet', include_top=False,input_shape = self.dim)        
        # add a global spatial average pooling layer
        x = base_model.output
        x = GlobalAveragePooling2D()(x)
        x = Dense(1024, activation='relu')(x) #changed to tanh when having geo features
        x= Dropout(0.5)(x) 
        '''
	#27 geometric features
        features= Input(shape=(50,),name='Features') #MM: scale spatial features between[-1,1]
        normFeat=Activation('tanh')(features)
        
        merged= concatenate([x,normFeat])
        '''
        predictions = Dense(self.n_classes, activation='softmax')(x)
        
        # this is the model we will train
        #model = Model(inputs=[base_model.input,features], outputs=predictions)
        model = Model(inputs=base_model.input, outputs=predictions)
        
        for layer in base_model.layers:
            layer.trainable = False
            
        #rmsprop=RMSprop(lr=0.00001, rho=0.9, epsilon=None, decay=0.0)    
        model.compile(optimizer='rmsprop', loss='categorical_crossentropy',metrics=['accuracy']) 
        #model.load_weights('../output/model_weights/segmentation/earlyFusion_R1.75_us+context/weights-improvement-50-0.0137.hdf5', by_name=False)         
        model.fit_generator(generator= my_training_generator,epochs=3, workers=multiprocessing.cpu_count(),
                            use_multiprocessing=True,shuffle=True,class_weight=None)                                                
        #*********************************************************************************************
        #featLayers=['Features', 'activation_1']    
        for layer in model.layers[:15]:
            #print layer.name
            layer.trainable = False
        for layer in model.layers[15:]:
           # if layer.name not in featLayers:
            if layer.name.startswith('Features') or layer.name.startswith('activation'):
                layer.trainable = False
            else:
                layer.trainable = True
                print(layer.name)
                        
        nadam=Nadam(lr= self.learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None, schedule_decay=0.004) # decay=0.004
        #sgd = SGD(lr=1e-3, decay=1e-6, momentum=0.9, nesterov=True) optimizer sgd?
        #rmsprop=RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)
        model.compile(optimizer=nadam, loss='categorical_crossentropy',metrics=['accuracy'])
    
        callbacks_list = self.defineCallbacks(['tensorboard','checkpoint','earlyStopping'])
                
        model.fit_generator(generator= my_training_generator,epochs=self.n_epochs,initial_epoch=0,validation_data=my_validation_generator,
                                    workers=multiprocessing.cpu_count(), use_multiprocessing=True,
                                    callbacks=callbacks_list,shuffle=True,class_weight=None) 
        return model        
