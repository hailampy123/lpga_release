#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 17:26:33 2018

@author: mxm7832
"""
from sklearn.utils import class_weight
import numpy as np   

def main():
    testLabelPath='../output_mm/typeset_symbols/test_labels.txt'
    testImgPath='../output_mm/typeset_symbols/testSymbols/'#'../output_mm/testSymbols_small_outline/'  
    trainLabelPath='../output_mm/typeset_symbols/train_labels.txt'#'../output_mm/train_labels.txt'
    trainImgPath='../output_mm/typeset_symbols/trainSymbols/'#'../output_mm/trainSymbols_small_outline/'
    validLabelPath='../output_mm/typeset_symbols/valid_labels.txt'

#update the klacels dictionary with numerical or  oneHot encoding    
def encodeLabels(trainlabels,testlabels=None):
    from sklearn import preprocessing
    from sklearn.utils import class_weight
   
    cat_train_labels=list(trainlabels.values()) 
    
    le = preprocessing.LabelEncoder()    
    le.fit(cat_train_labels)
    train_Nlabel = le.transform(cat_train_labels)
    #claculate train class weights
    classes=np.unique(train_Nlabel)
    cl_weights = class_weight.compute_class_weight('balanced',classes,train_Nlabel)
    
    enc = preprocessing.OneHotEncoder(sparse=False)
    train_Nlabel=train_Nlabel.reshape(len(train_Nlabel), 1)
    enc.fit(train_Nlabel)
    one_hot_label=enc.transform(train_Nlabel) 
    #update the training dictionary
    count=0
    for fileName, value in trainlabels.items():
        trainlabels[fileName]=value,one_hot_label[count]
        count +=1

    #transform the test labels if exist
    if testlabels:
        enc_testlabels= encode_testLabels(testlabels,(le,enc))
        return trainlabels,enc_testlabels,cl_weights
    
    return trainlabels,(le,enc),cl_weights

 
def encode_testLabels(testlabels,functions):
    func1,func2=functions
    
    test_Nlabel=func1.transform(list(testlabels.values()))
    test_Nlabel=test_Nlabel.reshape(len(test_Nlabel), 1)
    test_one_hot_label=func2.transform(test_Nlabel)           
    #update the test dictionary
    count=0
    for fileName, value in testlabels.items():
        testlabels[fileName]=value,test_one_hot_label[count]
        count +=1        
    return testlabels

def merge_similar_label(labels):
    for fileid,label in labels.items():
        
        if label in ['fractionalLine','minus','overline','hyphen']:
            labels[fileid]='minus'
        if label in ['ldots', 'cdots']:
            labels[fileid]= 'ldots'
        if label in ['dot','cdot','period']:
            labels[fileid]= 'dot'
    return labels
    
#MM: load spatial features from text files and generate a dic of numpy array
#TODO put it in extract cc script 
def loadFeat(path):
    txt=open(path)
    features={}
    for line in txt:
        parts = [p.strip(",") for p in line.strip().split(" ")]
        #MM: remove the par at the begin and end of feat vector
        parts[1]=parts[1][1:]
        parts[-1]=parts[-1][:-1]        
        feat=np.array([float(feat) for feat in parts[1:]])
        features[parts[0]]=feat
    return features     
               
#make a dictionary of labels
def load_label(labelpath,merge):
   text= open(labelpath)
   labels={}
   for line in text:
       labels[line.strip().split(', ')[0]]=line.strip().split(', ')[1]
   if merge:
       labels=merge_similar_label(labels)
     
   return labels
    

def infty_labels(trainLabelPath,testLabelPath,validLabelPath,merge=False):
    
    train_labels=load_label(trainLabelPath,merge)     
    train_labels,funcs,cl_weights=encodeLabels(train_labels)
    
    test_labels=load_label(testLabelPath,merge)
    test_labels=encode_testLabels(test_labels,funcs)

    valid_labels=load_label(validLabelPath,merge)    
    valid_labels=encode_testLabels(valid_labels,funcs)
    
    return train_labels,test_labels,valid_labels,funcs
#############################################################################################
#############################################################################################

def report_stats(testLabels,trainLabels):
    tr_labels=np.array(trainLabels.values())
    ts_labels=np.array(testLabels.values())
    
    classes=np.unique(tr_labels)
    
    class_stats={}
    #initialize the dic
    for cl in classes:
        class_stats[cl]=[0,0]
        
    for cl in tr_labels:
        class_stats[cl][0]=class_stats[cl][0]+1
                
    for cl in ts_labels:
        class_stats[cl][1]=class_stats[cl][1]+1  
    '''    
    fig, ax = plt.subplots(figsize=(15, 6))
    ax.plot([str(i) for i in classes], [i[0] for i in class_stats.values()], label="Train")
    ax.plot([str(i) for i in classes], [i[1] for i in class_stats.values() ], label="Test")
    ax.legend()
    plt.xticks([str(i) for i in classes], [str(i) for i in classes], rotation=90)
    plt.savefig('../output_mm/samples_per_class_prune.png',dpi=200)
    plt.show() 
    #print class_stats
    '''
    return class_stats

#write the new train and test list of remaind class on a text file            
def process_labels(trainLabelPath,testLabelPath,report=False):
    testLabels=load_label(testLabelPath) #dictionary of categorical
    trainLabels=load_label(trainLabelPath)
    if report:
        class_stats=report_stats(testLabels,trainLabels)
        for cl in class_stats.keys():
            train_samples=class_stats[cl][0]
            if train_samples<500:
                del class_stats[cl] #from 213 classes to 57 classes
        # prune train and test list
        main_cl=class_stats.keys()
        for k,v in testLabels.items():
            if v not in main_cl: del testLabels[k] 
                
        for k,v in trainLabels.items():
            if v not in main_cl: del trainLabels[k] 

        
    TrainLabels,TestLabels= encodeLabels(trainLabels,testLabels)
 
    return  TrainLabels,TestLabels
            
