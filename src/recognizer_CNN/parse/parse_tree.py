#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 19:15:01 2018

@author: mxm7832
"""
import networkx as nx
import networkx.algorithms as alg
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES


#class parse_tree():

    #def __init__(config):

def findRelationProbabilities(predict_probs,src,expressions):    
    for i in range(len(src)):
        #classes = np.array(predicted_probs[i])[:,0]
        #probs = np.array(predicted_probs[i])[:,1].astype(float)
        exprId, pid, cid = src[i]
        classes=predict_probs[exprId+'_'+pid+'_'+cid][1]
        probs=predict_probs[exprId+'_'+pid+'_'+cid][0]
        graph = expressions[exprId].expressionGraph
        graph.edge[pid][cid][EDGE_ATTRIBUTES.POSSIBLE_CLASSES] = classes
        graph.edge[pid][cid][EDGE_ATTRIBUTES.CLASS_CONFIDENCES] = probs

def setWeights( expr, classes,useGram=False):
    cls_index = {}

    #classes = ['RSUP', 'HORIZONTAL', 'NoRelation', 'RSUB', 'UNDER', 'LSUB', 'UPPER', 'LSUP','PUNC']
    graph = expr.expressionGraph
    for pid,cid,data in graph.edges(data=True):
        cls_index[(pid,cid)] = 0
        if data[EDGE_ATTRIBUTES.POSSIBLE_CLASSES][0] == "NoRelation":
                cls_index[(pid,cid)] += 1
        if cls_index[(pid,cid)] > len(classes) or data[EDGE_ATTRIBUTES.CLASS_CONFIDENCES][cls_index[(pid,cid)]] == 0:
            graph.remove_edge(pid,cid)
            del cls_index[(pid,cid)]
 
    for (pid,cid),index in cls_index.items():
        graph.edge[pid][cid]["weight"] = graph.edge[pid][cid][EDGE_ATTRIBUTES.CLASS_CONFIDENCES][index]
        graph[pid][cid][EDGE_ATTRIBUTES.PREDICTED_LAYOUT] = graph.edge[pid][cid][EDGE_ATTRIBUTES.POSSIBLE_CLASSES][index]
    expr.expressionGraph = graph

def parse_prob(expressions, source, predicted_probs, classes, extractTree=True):
    # get relation scores for the edges
    findRelationProbabilities(predicted_probs,source,expressions)
    # Make highest score the weight of each edge and record its class
    i = 0
    for exprId, expr in expressions.items():
        setWeights(expr,classes)
        if extractTree:
            # Perform Edmonds' algorithm and convert result to symbol graph
            try:
                mst = alg.maximum_spanning_arborescence(expr.expressionGraph, attr='weight', default=0.0)
            except nx.exception.NetworkXException:
                mst = expr.expressionGraph

            for n, data in expr.expressionGraph.nodes(data=True):
                mst.add_node(n,data)
            for pid,cid,data in expr.expressionGraph.edges(data=True):
                if mst.has_edge(pid,cid):
                    mst.add_edge(pid,cid,data)
            expr.expressionGraph = mst
        i += 1
        print(" Parsing: " + str(((i + 1) * 100) // len(expressions)) + "%", end="\r")
        