#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 15 18:55:50 2018

@author: mxm7832
"""
import keras
import numpy as np
#from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input  
from PIL import Image,ImageFilter
import cv2
import os

##############################################################################################################
def class_dic(task,classPath=None):
    cls_list=[]
    cls_dict={}
    if task == 'segment':
        cls_list=['SPLIT', 'MERGE']
        for i, cl in enumerate(cls_list):
            cls_dict[cl]=i

    if task=='symbol':
        if classPath:
            txt=open(classPath)
            for i,cl in enumerate(txt):
                cls_dict[cl.strip()]=i
                cls_list.append(cl.strip())
        else:
            print("list of symbol classes is not provided")
            
    if task == 'relation':
        cls_list=['RSUP', 'HORIZONTAL', 'NoRelation', 'RSUB', 'UNDER', 'LSUB', 'UPPER', 'LSUP','PUNC']
        for i, cl in enumerate(cls_list):
            cls_dict[cl]=i                    
    return cls_dict,cls_list
###############################################################################################################
class data_gen(keras.utils.Sequence):
    
    def __init__(self, img_path, labels, batch_size,dataset_name,feat=None):
        self.img_path=img_path
        self.file_names=[v for v in labels.keys()]
        self.labels= [v[1] for v in labels.values()]
        self.batch_size = batch_size
        self.dataset_name=dataset_name
        self.feat=feat

    def __len__(self):
        return int(np.ceil(len(self.file_names) / float(self.batch_size)))

    def __getitem__(self, idx):
        batch_filename = self.file_names[idx * self.batch_size:(idx + 1) * self.batch_size]        
        batch_label = self.labels[idx * self.batch_size:(idx + 1) * self.batch_size]
       # print(batch_filename,batch_label)
        batch_images=[] 
        features=[]   
        for imgname in batch_filename:
            if self.dataset_name=='infty':            
                im =Image.open(self.img_path+imgname+'.PNG')
                #im = im.filter(ImageFilter.BLUR)
            elif self.dataset_name=='crohme':   
                #im=Image.open(self.img_path+imgname.split('_')[0]+'/'+imgname.split('_')[1]+'.png')
                im=cv2.imread(self.img_path+imgname.split('_')[0]+'/'+imgname.split('_')[1]+'.png') 
                im=Image.fromarray(im, 'RGB')
                im = im.filter(ImageFilter.FIND_EDGES)

            im=(np.array(im)).astype('float32') / 255.0
            im = np.expand_dims(im, axis=0)
            im = preprocess_input(im)
            batch_images.extend(im)
            if self.feat:
                feat=self.feat[imgname]
                #replace negative values with zero(Relu)
                #feat[feat<0]=0
                features.append(feat)
                
        if features:
            return [np.array(batch_images),np.array(features)],np.array(batch_label)
        else:
            return np.array(batch_images),np.array(batch_label)

##################################################################################################################                
class data_gen_2D(keras.utils.Sequence):
    
    def __init__(self, img_path1,img_path2,labels, batch_size,dataset_name,feat=None):
        self.img_path1=img_path1
        self.img_path2=img_path2
        #hardcode path for now
        self.file_names= [file.strip()+'.PNG' for file in open('../output/CNN_img_test/testIMG_labels.txt')]
        self.labels=None
        if labels:
            self.file_names=[v+'.PNG' for v in labels.keys()]
            self.labels= [v[1] for v in labels.values()]
        self.batch_size = batch_size
        self.dataset_name=dataset_name
        self.feat=feat
    def __len__(self):        
        return int(np.ceil(len(self.file_names) / float(self.batch_size)))
            #return int(np.ceil(len(os.listdir(self.img_path1)) / float(self.batch_size)))
    def preProcess_img(self,im):
        #im = im.filter(ImageFilter.BLUR)
        im=(np.array(im)).astype('float32') / 255.0
        im = np.expand_dims(im, axis=0)
        img = preprocess_input(im)
        return img

    def __getitem__(self, idx):
        batch_filename = self.file_names[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_label=None
        if self.labels:        
            batch_label = self.labels[idx * self.batch_size:(idx + 1) * self.batch_size]
        #print(batch_filename)
        target_batch_images=[] 
        context_batch_images=[]
        features=[]
        for imgname in batch_filename:
            im = Image.open(self.img_path1+imgname)
            #im = im.filter(ImageFilter.BLUR)
            target_im=self.preProcess_img(im)
            #check if context exist for symbol:
            if not os.path.isfile(self.img_path2+imgname):
                print('context image not found')
                im = Image.open(self.img_path1+imgname)
            else:
                im= Image.open(self.img_path2+imgname)
                
            context_im=self.preProcess_img(im)

            target_batch_images.extend(target_im)
            context_batch_images.extend(context_im)
            if self.feat:
                feat=self.feat[imgname.split('.')[0]]
                #replace negative values with zero(Relu)
                #feat[feat<0]=0
                features.append(feat)

        if features and batch_label:
            return [np.array(target_batch_images),np.array(context_batch_images),np.array(features)],np.array(batch_label)
        elif not features:    
            return[np.array(target_batch_images),np.array(context_batch_images)],np.array(batch_label)
        elif not batch_label:
            return[np.array(target_batch_images),np.array(context_batch_images),np.array(features)]
            
#######################################################################################################################            
#feeding parent,child,context(us + 0.75 around) to 3 channel model   
class seg_gen_3D(keras.utils.Sequence):
    
    def __init__(self, CC_img_path,context_img_path,labels, batch_size,dataset_name,feat=None):
        self.context_img_path=context_img_path
        self.CC_img_path=CC_img_path
        self.file_names=[v for v in labels.keys()]
        self.labels= [v[1] for v in labels.values()]
        self.batch_size = batch_size
        self.dataset_name=dataset_name
        self.feat=feat
        
    def __len__(self):
        return int(np.ceil(len(self.file_names) / float(self.batch_size)))
    
    def preProcess_img(self,im):
        im = im.filter(ImageFilter.BLUR)
        im=(np.array(im)).astype('float32') / 255.0
        im = np.expand_dims(im, axis=0)
        img = preprocess_input(im)
        return img
    
    def __getitem__(self, idx):
        batch_filename = self.file_names[idx * self.batch_size:(idx + 1) * self.batch_size]        
        batch_label = self.labels[idx * self.batch_size:(idx + 1) * self.batch_size]
       # print(batch_filename,batch_label)
        parent_batch_images=[]
        child_batch_images=[] 
        context_batch_images=[]
        features=[]
        for imgname in batch_filename:
            parts=imgname.split('_')
            p_imgname=parts[0]+'_'+parts[1]
            c_imgname=parts[0]+'_'+parts[2]
            #if self.dataset_name=='infty':                        
            im = Image.open(self.CC_img_path+p_imgname+'.PNG')
            p_im=self.preProcess_img(im)
            parent_batch_images.extend(p_im)
            
            im = Image.open(self.CC_img_path+c_imgname+'.PNG')
            c_im=self.preProcess_img(im)
            child_batch_images.extend(c_im)
            
            im = Image.open(self.context_img_path+imgname+'.PNG')
            context_im=self.preProcess_img(im)
            context_batch_images.extend(context_im)
            
            if self.feat:
                feat=self.feat[imgname]
                #replace negative values with zero(Relu)
                #feat[feat<0]=0
                features.append(feat)

        if features:
            return [np.array(parent_batch_images),np.array(child_batch_images),
                    np.array(context_batch_images),np.array(features)],np.array(batch_label)
        else:    
            return [np.array(parent_batch_images),np.array(child_batch_images),
                    np.array(context_batch_images)],np.array(batch_label)