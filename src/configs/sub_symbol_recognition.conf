
# ======================================================================
# Training DATA CONFIGURATION
# ======================================================================

# Training Dataset
TRAINING_DATASET_NAME = ds_infty2_2Dhist_training
TRAINING_DATASET_PATH = ../Data/infty/InftyCDB-2/Training.txt

EXPERT_TYPE = 1

DEFAULT_NUMBER_OF_WORKERS = 8

PLOT_PATH = ../output/visuals/
# ======================================================================
# DATA PROCESSING CONFIGURATION
# ======================================================================

# Data loader
# INKML = load stroke data from .inkml files
# IMG = load image data from .PNG and .lg files
INPUT_DATA_LOADER_TYPE = IMG
INPUT_IMG_TYPE= IMG

GRAPH_LEVEL = symbol

SAVE_EXPRESSION_CONTEXT = 1

## Image Processing Parameters

IMAGE_PROCESSING_LG_PATH = ../Data/Expressions/LG/
IMAGE_PROCESSING_IMG_PATH = ../Data/Expressions/IMG/

# Trace Type
#   Stroke: traces made to resemble handwritten strokes.
#   Contour: Uses the contours of symbol as traces.
IMAGE_PROCESSING_TRACE_TYPE = Contour

IMAGE_PROCESSING_CONTOUR_REDUCTION_TYPE = Smooth
IMAGE_PROCESSING_CONTOUR_REDUCTION_VALUE = 1
IMAGE_PROCESSING_CONTOUR_REDUCTION_SMOOTHING_DISTANCE = 2

# ======================================================================
# PREPROCESSING CONFIGURATION
# ======================================================================

PREPROCESS_SMOOTHEN = CATMULL

NORMALIZE_TRACE_SETS = 0


# ======================================================================
# GRAPH CONFIGURATION
# ======================================================================

INITIAL_GRAPH_TYPE = sight
FIND_PUNCTUATION = 1

# ======================================================================
# FEATURE EXTRACTION CONFIGURATION
# ======================================================================
PARALLEL_FEATURE_EXTRACTION_WORKERS = 8

#####################################################################################

#######  PARAMETERS FOR SHAPE CONTEXT FEATURES

FEATURES_USE_SYMBOL_SHAPE_CONTEXT_FEATURES = 0
# Shape context circles format
# [List of circles]
# Circle = [number of shape context circles, number of angular sectors, distance between two circles, shape context method to use, Standard deviation factor, Sampling type, Normalization type]
# Note that number of shape context circles and number of angular sectors define the number of bins
# Number of bins = number of shape context circles x number of angular sectors
# A feature dimension will be added per bin. In case of inverse of the distance method a additional feature will be added for the center.
# SYMBOL_SHAPE_CONTEXT_CIRCLES = [[2, 4, 'UNIFORM', 'COUNT_POINTS'],
#                               + [3, 6, 'UNIFORM', 'PARZEN', 0.1, 'BIN_CENTER', 'POINT_BY_POINT']
#                               + [4, 5, 'UNIFORM', 'INVERSE_DISTANCE']]
# The above configuration will give 8(2x4) + 18(3x6) + 21(4x5 +1) = 37 features in total
SYMBOL_SHAPE_CONTEXT_CIRCLES = [[4, 10, 'UNIFORM', 'COUNT_POINTS']]

FEATURES_USE_EXPRESSION_SHAPE_CONTEXT_FEATURES = 1
# Shape context circles format
# [List of circles]
# Circle = [number of shape context circles, number of angular sectors, distance between two circles, shape context method to use, Standard deviation factor, Sampling type, Normalization type]
# Note that number of shape context circles and number of angular sectors define the number of bins
# Number of bins = number of shape context circles x number of angular sectors
# A feature dimension will be added per bin.
# EXPRESSION_SHAPE_CONTEXT_CIRCLES = [[2, 4, 'UNIFORM', 'COUNT_POINTS'],
#                                   + [4, 6, 'UNIFORM', 'PARZEN', 0.1, 'BIN_CENTER', 'POINT_BY_POINT']]
# The above configuration will give a 8(2x4) + 18(3x6) = 26 features in total
# If context is to be used, another 26 features will be computed for context
EXPRESSION_SHAPE_CONTEXT_CIRCLES = [[5, 14, 'UNIFORM', 'COUNT_POINTS', ]]

# SYMBOL_FARTHEST_PT, BOUNDING_BOX_DIAGONAL, NEAREST_NEIGHBOR, FARTHEST_NEIGHBOR
EXPRESSION_SHAPE_CONTEXT_RADIUS_METHOD = ['SYMBOL_FARTHEST_PT']
# PARAMETER USED TO DEFINE THE AMOUNT OF CONTEXT TO BE USED
EXPRESSION_SHAPE_CONTEXT_RADIUS_FACTOR = [8]
# CONTEXT_ONLY, SYMBOL_ONLY, CONTEXT_AND_SYMBOL
EXPRESSION_SHAPE_CONTEXT_USE_GLOBAL_CONTEXT = ['CONTEXT_ONLY']

# For visualization
# 0 - relative and 1 - absolute
SHAPE_CONTEXT_INTENSITY_TYPE = 1
# 1 - debug mode on
SHAPE_CONTEXT_DEBUG_MODE = 0


# ==============================================================================================================================
FEATURES_USE_CROSSINGS = 0
CROSSINGS_NUMBER = 5
CROSSINGS_OVERLAPPING = 0
CROSSINGS_SUBCROSSINGS = 13
SUBCROSSINGS_WEIGHTS = 0 # 0 - Uniform, 1 - Gaussian
SUBCROSSINGS_GAUSS_SIGMA = 0.5
CROSSINGS_USE_COUNT = 1
CROSSINGS_USE_MIN = 1
CROSSINGS_USE_MAX = 1

FEATURES_USE_DIAGONAL_CROSSINGS = 0
DIAGONAL_CROSSINGS_NUMBER = 5
DIAGONAL_CROSSINGS_OVERLAPPING = 0
DIAGONAL_CROSSINGS_SUBCROSSINGS = 13
DIAGONAL_SUBCROSSINGS_WEIGHTS = 0 # 0 - Uniform, 1 - Gaussian
DIAGONAL_SUBCROSSINGS_GAUSS_SIGMA = 0.5
DIAGONAL_CROSSINGS_USE_COUNT = 1
DIAGONAL_CROSSINGS_USE_MIN = 1
DIAGONAL_CROSSINGS_USE_MAX = 1

# Angular Crossings
ANGULAR_CROSSINGS_NUMBER = 4
ANGULAR_CROSSINGS_SUBCROSSINGS = 1
ANGULAR_SUBCROSSINGS_WEIGHTS = 0 # 0 - Uniform, 1 - Gaussian
ANGULAR_SUBCROSSINGS_GAUSS_SIGMA = 0.5
ANGULAR_CROSSINGS_USE_COUNT = 1
ANGULAR_CROSSINGS_USE_MIN = 1
ANGULAR_CROSSINGS_USE_MAX = 1

# Zoning Crossings
# Grid format
# [List of grids]
# Grid = [rows, cols, % width covered by grid, % height covered by grid, offset on x (%), offset on y (%)]
ZONING_CROSSINGS_GRIDS = [[3, 3, 1.0, 1.0, 0.0, 0.0], [2, 2, 0.667, 0.667, 0.167, 0.167]]
ZONING_CROSSINGS_WEIGHTS = 0 # 0 - normal, 1 - gauss
ZONING_CROSSINGS_GAUSS_SIGMA = 0.5
ZONING_CROSSINGS_HOR_LINES = 100
ZONING_CROSSINGS_VER_LINES = 100
ZONING_CROSSINGS_DIA_LINES = 100

FEATURES_USE_2D_HISTOGRAMS = 1
# Grid format
# [List of grids]
# Grid = [rows, cols, % width, % height, offset x (%), offset y (%), Use Lenghts (True) or Points (False), Use Parzen (True) for membership, Standard Deviation factor for Parzen ]
# Note that rows, cols represent corners on each direction
# The grid will have (rows - 1) * (cols - 1) cells
# A feature dimension will be added per corner of the grid
# 2D_HISTOGRAMS_GRIDS = [[5, 5, 1.0, 1.0, 0.0, 0.0, True, False, 0.1], [4, 4, 0.8, 0.8, 0.1, 0.1, True, False, 0.1]]
2D_HISTOGRAMS_GRIDS = [[8, 8, 1.0, 1.0, 0.0, 0.0, False, 1, False, 0.1]]

FEATURES_USE_ORIENTATION_HISTOGRAMS = 0
# Grid format
# [List of grids]
# Grid = [rows, cols, % width, % height, offset x (%), offset y (%), #Orientations]
ORIENTATION_HISTOGRAMS_GRIDS = [[5, 5, 1.0, 1.0, 0.0, 0.0, 4]]


# Temporal Features (Describe drawing process)
TEMPORAL_FEATURES_NUM_TRACES = 0
TEMPORAL_FEATURES_TRACE_LENGTH = 0
TEMPORAL_FEATURES_CONNECT_LENGTH = 0
TEMPORAL_FEATURES_TOTAL_LENGTH = 0

# General Trace Description Features
FEATURES_USE_TRACES_DESCRIPTION = 0

# General Trace Description Features
TRACES_DESCRIPTION_USE_COUNT = 1
TRACES_DESCRIPTION_USE_ASPECT_RATIO = 1

TRACES_DESCRIPTION_USE_TOTAL_ANGULAR_CHANGE = 0
TRACES_DESCRIPTION_USE_AVERAGE_ANGULAR_CHANGE = 1

TRACES_DESCRIPTION_USE_TOTAL_LINE_LENGTH = 0
TRACES_DESCRIPTION_USE_AVERAGE_LINE_LENGTH = 1

TRACES_DESCRIPTION_USE_TOTAL_SHARP_POINTS = 0
TRACES_DESCRIPTION_USE_AVERAGE_SHARP_POINTS = 1

TRACES_DESCRIPTION_USE_VARIANCE_X = 0
TRACES_DESCRIPTION_USE_VARIANCE_Y = 0
TRACES_DESCRIPTION_USE_COVARIANCE_XY = 0

# Not in the original set of trace description features
TRACES_DESCRIPTION_USE_MID_POINT_VARIANCE_X = 1
TRACES_DESCRIPTION_USE_MID_POINT_VARIANCE_Y = 1
TRACES_DESCRIPTION_USE_MID_POINT_COVARIANCE_XY = 1

TRACES_DESCRIPTION_TOTAL_ORIGINAL_POINTS = 1

# ======================================================================
# Classifier configuration
# ======================================================================

# Classifier Parameters
# - Classifier type
#   * 1 = Random Forest
#   * 2 = Linear Support Vector Machine
#   * 3 = Radial Basis Function Support Vector Machine
CLASSIFIER_TYPE = 1 # 3?

CLASSIFIER_TRAINING_TIMES = 1

CLASSIFIER_CROSSVALIDATION_PARTITIONS = 5


# Classifier files output directory
CLASSIFIER_DIR = ../output/classifiers
CLASSIFIER_NAME = rf_infty2_2Dhist

RANDOM_FOREST_N_TREES = 50 # 50, 100, 200
RANDOM_FOREST_MAX_DEPTH = 15 # 12, 15, 18
RANDOM_FOREST_MAX_FEATURES = 30 # 10, 30, 50
RANDOM_FOREST_CRITERION = 0 # 0 = ENTROPY, 1 = GINI
RANDOM_FOREST_JOBS = 8

LINEAR_SVM_PROBABILISTIC = 0

RBF_SVM_PROBABILISTIC = 0
RBF_SVM_C = 10.0
RBF_SVM_GAMMA = 0.001
