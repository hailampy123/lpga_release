
import time
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from IO.write_output import write_lgfile
from recognizer.data.feature_dataset import FeatureDataset
from recognizer.evaluation.eval_ops import EvalOps
from recognizer.recognition.symbol_classifier import SymbolClassifier

class ClassifierTrainer:
    @staticmethod
    def load_training_set(config):
        training_filename = config.data["DATASET_DIR"] + "/" + config.data["TRAINING_DATASET_NAME"] + ".dat"
        return FeatureDataset.load_from_file(training_filename)

    @staticmethod
    def load_testing_set(config):
        testing_filename = config.data["DATASET_DIR"] + "/" + config.data["TESTING_DATASET_NAME"] + ".dat"
        return FeatureDataset.load_from_file(testing_filename)


    @staticmethod
    def train_random_forest(training_dataset, config):
        # check parameters
        try:
            n_trees = config.get_int("RANDOM_FOREST_N_TREES", -1)
            if n_trees < 1:
                raise Exception("Invalid <RANDOM_FOREST_N_TREES> value")
        except:
            raise Exception("Invalid <RANDOM_FOREST_N_TREES> value")

        try:
            max_D = config.get_int("RANDOM_FOREST_MAX_DEPTH", -1)
            if max_D < 1:
                raise Exception("Invalid <RANDOM_FOREST_MAX_DEPTH> value")
        except:
            raise Exception("Invalid <RANDOM_FOREST_MAX_DEPTH> value")

        try:
            if config.get_str("RANDOM_FOREST_MAX_FEATURES", "-1").lower() == "auto" \
                    or config.get_str("RANDOM_FOREST_MAX_FEATURES", "-1").lower() == "sqrt":
                max_features = config.get_str("RANDOM_FOREST_MAX_FEATURES").lower()
            else:
                max_features = config.get_int("RANDOM_FOREST_MAX_FEATURES", -1)
                if max_features < 1:
                    raise Exception("Invalid <RANDOM_FOREST_MAX_FEATURES> value")
                if max_features > training_dataset.num_attributes():
                    print("Warning: Training dataset has less features than [RANDOM_FOREST_MAX_FEATURES]")
                    max_features = training_dataset.num_attributes()
        except:
            raise Exception("Invalid <RANDOM_FOREST_MAX_FEATURES> value")

        try:
            criterion_t = 'gini' if config.get_int("RANDOM_FOREST_CRITERION", 0) == 1 else 'entropy'
        except:
            raise Exception("Invalid <RANDOM_FOREST_MAX_FEATURES> value")

        try:
            num_jobs = config.get_int("RANDOM_FOREST_JOBS", 1)
        except:
            raise Exception("Invalid <RANDOM_FOREST_JOBS> value")

        # create and train classifier
        forest = RandomForestClassifier(n_estimators=n_trees,criterion=criterion_t,
                                        max_features=max_features, max_depth=max_D,n_jobs=num_jobs)

        assert isinstance(training_dataset, FeatureDataset)
        forest.fit(training_dataset.data, np.ravel(training_dataset.labels))

        # classifier = SymbolClassifier(SymbolClassifier.TypeRandomForest, forest, training_dataset.label_mapping,
        #                               training_dataset.class_mapping, config, training_dataset.normalizer, True)
        classifier = forest

        return classifier

    @staticmethod
    def train_linear_svm(training_dataset, config):
        try:
            make_probabilistic = config.get_bool("LINEAR_SVM_PROBABILISTIC", 1)
        except:
            raise Exception("Invalid <LINEAR_SVM_PROBABILISTIC> value")

        linear_svm = svm.SVC(kernel='linear', probability=make_probabilistic)
        assert isinstance(training_dataset, FeatureDataset)
        linear_svm.fit(training_dataset.data, np.ravel(training_dataset.labels))

        # classifier = SymbolClassifier(SymbolClassifier.TypeSVMLIN, linear_svm, training_dataset.label_mapping,
        #                               training_dataset.class_mapping, config, training_dataset.normalizer, make_probabilistic)
        classifier = linear_svm
        return classifier

    @staticmethod
    def train_rfb_svm(training_dataset, config):
        try:
            make_probabilistic = config.get_bool("RBF_SVM_PROBABILISTIC", 1)
        except:
            raise Exception("Invalid <RBF_SVM_PROBABILISTIC> value")

        try:
            rbf_C = config.get_float("RBF_SVM_C", 1)
        except:
            raise Exception("Invalid <RBF_SVM_C> value")

        try:
            rbf_gamma = config.get_float("RBF_SVM_GAMMA", 1)
        except:
            raise Exception("Invalid <RBF_SVM_GAMMA> value")

        rbf_svm = svm.SVC(kernel='rbf', C=rbf_C, gamma=rbf_gamma, probability=make_probabilistic)

        assert isinstance(training_dataset, FeatureDataset)
        rbf_svm.fit(training_dataset.data, np.ravel(training_dataset.labels))

        # classifier = SymbolClassifier(SymbolClassifier.TypeSVMRBF, rbf_svm, training_dataset.label_mapping,
        #                               training_dataset.class_mapping, config, training_dataset.normalizer, make_probabilistic)
        classifier = rbf_svm

        return classifier


    @staticmethod
    def train_classifier(training_dataset, config, get_raw_classifier=False):
        classifier_type = config.get_int("CLASSIFIER_TYPE", 0)

        if classifier_type == SymbolClassifier.TypeRandomForest:
            raw_classifier = ClassifierTrainer.train_random_forest(training_dataset, config)
        elif classifier_type == SymbolClassifier.TypeSVMLIN:
            raw_classifier = ClassifierTrainer.train_linear_svm(training_dataset, config)
        elif classifier_type == SymbolClassifier.TypeSVMRBF:
            raw_classifier = ClassifierTrainer.train_rfb_svm(training_dataset, config)
        else:
            raise Exception("Must select a valid classifier type in the configuration file")

        if get_raw_classifier:
            return raw_classifier
        else:
            if classifier_type == SymbolClassifier.TypeRandomForest:
                classifier = SymbolClassifier(SymbolClassifier.TypeRandomForest, raw_classifier, training_dataset.label_mapping,
                                      training_dataset.class_mapping, config, training_dataset.normalizer, True)
            elif classifier_type == SymbolClassifier.TypeSVMLIN:
                try:
                    make_probabilistic = config.get_bool("LINEAR_SVM_PROBABILISTIC", 1)
                except:
                    raise Exception("Invalid <LINEAR_SVM_PROBABILISTIC> value")
                classifier = SymbolClassifier(SymbolClassifier.TypeSVMLIN, raw_classifier, training_dataset.label_mapping,
                                      training_dataset.class_mapping, config, training_dataset.normalizer, make_probabilistic)
            elif classifier_type == SymbolClassifier.TypeSVMRBF:
                try:
                    make_probabilistic = config.get_bool("RBF_SVM_PROBABILISTIC", 1)
                except:
                    raise Exception("Invalid <RBF_SVM_PROBABILISTIC> value")
                classifier = SymbolClassifier(SymbolClassifier.TypeSVMRBF, raw_classifier, training_dataset.label_mapping,
                                          training_dataset.class_mapping, config, training_dataset.normalizer, make_probabilistic)
            else:
                raise Exception("Must select a valid classifier type in the configuration file")

        return classifier

    @staticmethod
    def train_crossvalidation(training_dataset, config, verbose=False, verbose_last='\n'):
        # ... get partitions
        n_partitions = config.get_int("CLASSIFIER_CROSSVALIDATION_PARTITIONS", -1)
        is_lg_generation = config.get_bool("CLASSIFIER_LG_GENERATION",False)
        parts_data, parts_labels, parts_indices, parts_sources = training_dataset.split_data(n_partitions)

        local_training_accuracies = np.zeros(n_partitions)
        local_training_averages = np.zeros(n_partitions)
        local_training_stdevs = np.zeros(n_partitions)

        local_evaluation_accuracies = np.zeros(n_partitions)
        local_evaluation_averages = np.zeros(n_partitions)
        local_evaluation_stdevs = np.zeros(n_partitions)

        label_mapping = training_dataset.label_mapping
        class_mapping = training_dataset.class_mapping

        total_training_time = 0
        total_evaluation_time = 0

        best_classifier_ref = None
        best_accuracy = None

        offset_partition = 0
        all_errors = []
        for i in range(n_partitions):
            if verbose:
                print("CROSS-VALIDATION: [" + ("#" * i) + ("-" * (n_partitions - i)) + "]", end="\r")
            # prepare datasets ...
            training_data = np.concatenate(parts_data[:i] + parts_data[i + 1:], axis=0)
            training_labels = np.concatenate(parts_labels[:i] + parts_labels[i + 1:], axis=0)

            testing_data = parts_data[i]
            testing_labels = parts_labels[i]
            testing_indices = parts_indices[i]
            testing_sources = parts_sources[i]

            training_dataset = FeatureDataset(training_data, training_labels, label_mapping, class_mapping)
            testing_dataset = FeatureDataset(testing_data, testing_labels, label_mapping, class_mapping, testing_sources)

            start_training_time = time.time()
            classifier = ClassifierTrainer.train_classifier(training_dataset, config)

            end_training_time = time.time()
            total_training_time += end_training_time - start_training_time

            start_training_time = time.time()
            # training ...
            training_predicted, eval_training_metrics = EvalOps.classifier_per_class_accuracy(classifier, training_dataset, get_output_data=True);
            accuracy, per_class_avg, per_class_stdev, errors = eval_training_metrics

            local_training_accuracies[i] = accuracy
            local_training_averages[i] = per_class_avg
            local_training_stdevs[i] = per_class_stdev

            # testing ...
            testing_predicted, eval_testing_metrics = EvalOps.classifier_per_class_accuracy(classifier, testing_dataset,get_output_data=True)
            accuracy, per_class_avg, per_class_stdev, errors = eval_testing_metrics
            #write the results of the data predicted to lg files
            if(is_lg_generation):
                write_lgfile(testing_dataset, testing_predicted)

            local_evaluation_accuracies[i] = accuracy
            local_evaluation_averages[i] = per_class_avg
            local_evaluation_stdevs[i] = per_class_stdev

            # save to complete list of errors
            for idx, expected, predicted, zero in errors:
                expected_class = training_dataset.label_mapping[expected]
                predicted_class = training_dataset.label_mapping[predicted]

                all_errors.append((int(testing_indices[idx]), expected_class, predicted_class, i))

            end_training_time = time.time()
            total_evaluation_time += end_training_time - start_training_time

            if best_classifier_ref is None or best_accuracy < local_training_accuracies[i]:
                best_classifier_ref = classifier
                best_accuracy = local_training_accuracies[i]

            offset_partition += testing_data.shape[0]

        if verbose:
            print("CROSS-VALIDATION: [" + ("#" * n_partitions) + "]", end=verbose_last)

        training_results = (local_training_accuracies, local_training_averages, local_training_stdevs)
        evaluation_results = (local_evaluation_accuracies, local_evaluation_averages, local_evaluation_stdevs)
        times = (total_training_time, total_evaluation_time)

        return training_results, evaluation_results, times, best_classifier_ref, all_errors
