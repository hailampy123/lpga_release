
import math
import numpy as np
from concurrent.futures import ProcessPoolExecutor


class EvalOps:
    Predicted_Output = None

    @staticmethod
    def predict_in_chunks(data, symbol_classifier, chunk_size, workers=1):
        classifier = symbol_classifier.trained_classifier

        all_chunks = []
        n_samples = np.size(data, 0)
        n_chunks = math.ceil(n_samples / float(chunk_size))
        for idx in range(n_chunks):
            start = idx * chunk_size
            last = (idx + 1) * chunk_size
            all_chunks.append(data[start:last])

        all_predicted = np.zeros(n_samples)
        if workers == 1:
            for idx, current_features in enumerate(map(classifier.predict, all_chunks)):
                start = idx * chunk_size
                last = (idx + 1) * chunk_size
                #print(str((start, last)), flush=True)

                all_predicted[start:last] = current_features
        else:
            with ProcessPoolExecutor(max_workers=workers) as executor:
                for idx, current_features in enumerate(executor.map(classifier.predict, all_chunks)):
                    start = idx * chunk_size
                    last = (idx + 1) * chunk_size
                    all_predicted[start:last] = current_features

        return all_predicted

    @staticmethod
    def predict_topk_in_chunks(data, symbol_classifier, topk, chunk_size):
        n_samples = np.size(data, 0)
        all_confidences = []
        pos = 0
        while pos < n_samples:
            last = min(pos + chunk_size, n_samples)

            # ...get predictions for current chuck...
            chunk_confidences = symbol_classifier.predict_proba(data[pos:last, :], topk)

            # ...add them to result...
            all_confidences += chunk_confidences

            pos = last

        return all_confidences

    @staticmethod
    def count_elements_per_class(labels, label_mapping):
        n_samples = labels.shape[0]

        counts_per_class = {}
        for k in range(n_samples):
            expected = label_mapping[int(labels[k, 0])]

            if expected in counts_per_class:
                counts_per_class[expected] += 1
            else:
                counts_per_class[expected] = 1

        return counts_per_class

    @staticmethod
    def count_errors(predicted, labels, n_classes):
        counts_per_class = np.zeros(n_classes)
        errors_per_class = np.zeros(n_classes)
        total_correct = 0
        n_samples = np.size(labels, 0)
        all_errors = []
        for k in range(n_samples):
            expected = labels[k, 0]

            counts_per_class[expected] += 1

            if predicted[k] == expected:
                total_correct += 1
            else:
                errors_per_class[expected] += 1
                all_errors.append((k, expected, predicted[k], 0))

        return total_correct, counts_per_class, errors_per_class, all_errors



    @staticmethod
    def count_errors_from_error_list(error_list):
        count_per_class = {}
        for idx, expected, predicted, fold in error_list:
            if not expected in count_per_class:
                count_per_class[expected] = 1
            else:
                count_per_class[expected] += 1

        return count_per_class

    @staticmethod
    def per_class_accuracy(predicted, labels, n_classes):
        n_training_samples = labels.shape[0]

        total_correct, counts_per_class, errors_per_class, all_errors = EvalOps.count_errors(predicted, labels, n_classes)
        global_accuracy = (float(total_correct) / float(n_training_samples)) * 100.0


        # only consider classes that existed in evaluation dataset...
        valid_classes = counts_per_class > 0

        all_accuracies = 1.0 - errors_per_class[valid_classes] / counts_per_class[valid_classes]
        per_class_avg = all_accuracies.mean() * 100.0
        per_class_stdev = all_accuracies.std() * 100.0

        return global_accuracy, per_class_avg, per_class_stdev, all_errors

    @staticmethod
    def classifier_per_class_accuracy(symbol_classifier, feature_dataset, chunk_size=5000, workers=1, get_output_data=False):
        # now, compute training evaluation metrics
        # ... predict for training set ....
        symbol_classifier.mask_data(feature_dataset)
        predicted = symbol_classifier.predictClassNames(feature_dataset)
        '''predicted = EvalOps.predict_in_chunks(
            feature_dataset.data, symbol_classifier, chunk_size, workers)  # 100'''
        # ... compute metrics...
        n_classes = feature_dataset.num_classes()

        if(get_output_data):
            return predicted, EvalOps.per_class_accuracy(predicted, feature_dataset.labels, n_classes)
        return EvalOps.per_class_accuracy(predicted, feature_dataset.labels, n_classes)

    @staticmethod
    def classifier_per_class_accuracy_exclude(symbol_classifier, feature_dataset, exclude_value,chunk_size=5000, normalize_confidence=False, get_output_data=False):
        #get the class-label and probability of the class with highest confidence
        label_confidence = EvalOps.predict_topk_in_chunks(feature_dataset.data, symbol_classifier, topk=2, chunk_size=chunk_size)  # 100
        predicted =[]
        confidences=[]
        for lc in label_confidence:
            predicted_0, confidence_0 = lc[0]
            predicted_1, confidence_1 = lc[1]
            if normalize_confidence:
                total_confidence= confidence_0+confidence_1
                confidence_0, confidence_1 = confidence_0/total_confidence, confidence_1/total_confidence
            current_predicted = predicted_1 if predicted_0 == exclude_value else predicted_0
            current_confidence = confidence_1 if predicted_0 == exclude_value else confidence_0
            predicted.append(current_predicted)
            confidences.append(current_confidence)

        # ... compute metrics...
        n_classes = feature_dataset.num_classes()
        if (get_output_data):
            return predicted, confidences, EvalOps.per_class_accuracy(predicted, feature_dataset.labels, n_classes)
        return EvalOps.per_class_accuracy(predicted, feature_dataset.labels, n_classes)

    @staticmethod
    def classifier_prob_per_class_accuracy(symbol_classifier, feature_dataset, chunk_size=5000, get_output_data=False):
        # get the class-label and probability of the class with highest confidence
        label_confidence = EvalOps.predict_topk_in_chunks(feature_dataset.data, symbol_classifier, topk=1,
                                                          chunk_size=chunk_size)  # 100
        predicted = []
        confidences = []
        for lc in label_confidence:
            predicted_i, confidence_i = lc[0]
            predicted.append(predicted_i)
            confidences.append(confidence_i)
        # ... compute metrics...
        n_classes = feature_dataset.num_classes()
        if (get_output_data):
            return predicted, confidences, EvalOps.per_class_accuracy(predicted, feature_dataset.labels, n_classes)
        return EvalOps.per_class_accuracy(predicted, feature_dataset.labels, n_classes)

    @staticmethod
    def create_confusion_matrix(expected_list, predicted_list, label_mapping):
        confusion_matrix = {}
        for expected_class, predicted_class in zip(expected_list, predicted_list):
            # in case that labels are provided (no actual class names)
            expected_class = label_mapping[int(expected_class[0])]
            predicted_class = label_mapping[int(predicted_class)]

            if expected_class not in confusion_matrix:
                confusion_matrix[expected_class] = {}
            if predicted_class not in confusion_matrix:
                confusion_matrix[predicted_class] = {}

            if predicted_class not in confusion_matrix[expected_class]:
                confusion_matrix[expected_class][predicted_class] = 1
            else:
                confusion_matrix[expected_class][predicted_class] += 1

        return confusion_matrix


    @staticmethod
    def copy_confusion_matrix(original_matrix):
        new_matrix = {}
        # create copies of the inner dictionaries
        for name in original_matrix:
            new_matrix[name] = dict(original_matrix[name])

        return new_matrix

    @staticmethod
    def delete_confusion_matrix_entries(confusion_matrix, entries_lists):
        for class_name in entries_lists:
            if class_name in confusion_matrix:
                for other_class in entries_lists[class_name]:
                    if other_class in confusion_matrix[class_name]:
                        del confusion_matrix[class_name][other_class]

                if len(confusion_matrix[class_name]) == 0:
                    del confusion_matrix[class_name]

    @staticmethod
    def confusion_matrix_sum(confusion_matrix):
        total = 0
        # create copies of the inner dictionaries
        for name in confusion_matrix:
            for other in confusion_matrix[name]:
                total += confusion_matrix[name][other]

        return total

    @staticmethod
    def confusion_matrix_top_n_entries(confusion_matrix, top_n):
        all_entries = []
        for name in confusion_matrix:
            all_entries += [(confusion_matrix[name][other], name, other) for other in confusion_matrix[name]]

        all_entries = sorted(all_entries, reverse=True)

        return all_entries[:top_n]

    @staticmethod
    def find_error_clusters(confusion_matrix):
        class_clusters = []

        for expected_class in confusion_matrix:
            # find current cluster ..
            current_expected_cluster = -1
            for idx, cluster in enumerate(class_clusters):
                if expected_class in cluster:
                    current_expected_cluster = idx
                    break

            if current_expected_cluster == -1:
                # not in any cluster yet, create new cluster and add the class ...
                current_expected_cluster = len(class_clusters)
                class_clusters.append([expected_class])

            # now, for each class that has a confusion ...
            for predicted_class in confusion_matrix[expected_class]:
                # find cluster of predicted class
                current_pred_cluster = -1
                for idx, cluster in enumerate(class_clusters):
                    if predicted_class in cluster:
                        current_pred_cluster = idx
                        break

                # only if they are on diffent clusters ...
                if current_expected_cluster != current_pred_cluster:
                    if current_pred_cluster == -1:
                        # easy case, class is not in any cluster, add to expected class cluster
                        class_clusters[current_expected_cluster].append(predicted_class)
                    else:
                        min_idx = min(current_expected_cluster, current_pred_cluster)
                        max_idx = max(current_expected_cluster, current_pred_cluster)

                        # add all elements from largest idx cluster to smallest idx cluster
                        class_clusters[min_idx] += class_clusters[max_idx]

                        # remove largest idx
                        del class_clusters[max_idx]

                        # ensure that expected cluster is the merged cluster
                        current_expected_cluster = min_idx

        return class_clusters

    @staticmethod
    def load_similar_shapes(filename):
        # load file
        similar_shapes_file = open(filename, "r")
        lines = similar_shapes_file.readlines()
        similar_shapes_file.close()

        similar_shapes = {}
        for line in lines:
            parts = line.strip().split(",")
            if len(parts) == 2:
                shape_1 = parts[0]
                shape_2 = parts[1]

                if shape_1 not in similar_shapes:
                    similar_shapes[shape_1] = []
                if shape_2 not in similar_shapes:
                    similar_shapes[shape_2] = []

                if shape_2 not in similar_shapes[shape_1]:
                    similar_shapes[shape_1].append(shape_2)
                if shape_1 not in similar_shapes[shape_2]:
                    similar_shapes[shape_2].append(shape_1)

        return similar_shapes

