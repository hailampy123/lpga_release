from recognizer.evaluation.eval_ops import EvalOps
import pickle
import numpy as np

class TestClassification:

    '''
    This class contains all methods to test a classifier on a dataset
    '''

    @staticmethod
    def predict_classes_from_file(classifier_filename, eval_dataset):
        in_file = open(classifier_filename, 'rb')
        classifier = pickle.load(in_file)
        in_file.close()
        #predict the output from the classifier
        return TestClassification.predict_classes(classifier, eval_dataset)

    @staticmethod
    def predict_classes(classifier, eval_dataset):
        # predict the output from the classifier
        predicted_output, evaluation_metrics = EvalOps.classifier_per_class_accuracy(classifier, eval_dataset,
                                                                                     get_output_data=True)
        return predicted_output

    @staticmethod
    def predict_classes_with_confidence_from_file(classifier_filename, eval_dataset):
        in_file = open(classifier_filename, 'rb')
        classifier = pickle.load(in_file)
        in_file.close()
        #predict the output from the classifier
        predicted_output, confidence, evaluation_metrics = EvalOps.classifier_prob_per_class_accuracy(classifier, eval_dataset, get_output_data=True)
        return predicted_output, confidence, evaluation_metrics

    @staticmethod
    def predict_classes_with_confidence(classifier, eval_dataset):
        #predict the output from the classifier
        predicted_output, confidence, evaluation_metrics = EvalOps.classifier_prob_per_class_accuracy(classifier, eval_dataset, get_output_data=True)
        return np.array(predicted_output), np.array(confidence)

    @staticmethod
    def predict_classes_and_confidence_except(classifier, eval_dataset, except_value):
        # predict the output from the classifier, ignoring one class value
        predicted_output, confidence, evaluation_metrics = EvalOps.classifier_per_class_accuracy_exclude(classifier,eval_dataset, except_value, get_output_data=True, normalize_confidence=False)
        return predicted_output, confidence, evaluation_metrics

    @staticmethod
    def predict_layout_classes_with_confidence_from_file(classifier_filename, eval_dataset):
        in_file = open(classifier_filename, 'rb')
        classifier = pickle.load(in_file)
        in_file.close()
        # predict the output from the classifier
        predicted_output, confidence, evaluation_metrics = EvalOps.classifier_per_class_accuracy_exclude(classifier,
                                                                                                         eval_dataset,
                                                                                                         "NoRelation",
                                                                                                         get_output_data=True)
        return predicted_output, confidence, evaluation_metrics
