import math
from concurrent.futures import ProcessPoolExecutor
import numpy as np
from recognizer.util.math_ops import MathOps
class Preprocessor:

    def __init__(self, configuration):
        self.config = configuration
        self.smoothening_type = self.config.get_str("TRACE_SMOOTHENING_TYPE", "catmull")
        self.debug_raw = self.config.get_str("DEBUG_RAW_PREFIX", None)
        self.debug_added = self.config.get_str("DEBUG_ADDED_PREFIX", None)
        self.debug_smooth = self.config.get_str("DEBUG_SMOOTH_PREFIX", None)
        #MM added debug for traces after normalization
        self.debug_norm = self.config.get_str("DEBUG_NORM_TRACE", None)
        self.normalize = self.config.get_bool("NORMALIZE_TRACE_SETS", False)

    def removeDuplicatedPoints(self, trace):
        minX, maxX, minY, maxY = trace.getBoundaries()
        w = maxX - minX
        h = maxY - minY
        diagonal = math.sqrt( w * w + h * h )

        i = 0
        while i < len(trace.points):
            j = i + 1
            while j < len(trace.points):
                if trace.distance(i, j) > diagonal * 0.1:
                    break

                if trace.points[i][0] == trace.points[j][0] and trace.points[i][1] == trace.points[j][1]:
                    del trace.points[j]
                else:
                    j += 1
            i += 1

    def normalizeEntireEquation(self, equation):
        '''
        Normalize every symbol in height of 200 preserving the aspect ratio
        '''

        # Find the entire bound of the equation
        min_x, min_y, max_x, max_y = equation.boundingBox
        eq_height = (max_y - min_y)
        normalize_ratio = 200.0 / eq_height

        ## get the new points after normalization
        stroke_sets = equation.componentMap.values()
        for symbol in stroke_sets:
            # initialize normalized trace
            # calculate for every trace in the symbol
            for trace in symbol.traces:
                points = np.array(trace.points)
                # translate the coordinates of the points to the left aligned
                points[:, 0] -= min_x
                points[:, 1] -= min_y
                # scale the coordinates of the points to a given ratio
                points *= normalize_ratio
                trace.points = points.tolist()
                trace.updateBoundaries()
            symbol.updateTracesBoundingBox()

        equation.offsets, equation.sizes = equation.intializeSizesOffsets(equation.componentMap)

    # uses the algorithm defined in [1] to find the sharp points of the trace...
    # returns a list of tuples of the form (index, (x, y))  for all the sharp points
    def getSharpPoints(self, trace):
        # the first is a sharp point
        sharpPoints = [(0, trace.points[0])]

        # now calculate the slope angles between each pair of consecutive points...
        alpha = []
        for i in range(len(trace.points) - 1):
            alpha.append(MathOps.slopeAngle(trace.points[i], trace.points[i + 1]))

        # check
        if len(alpha) <= 1:
            # very special case where the trace is one single point
            # no more sharp points than itself...
            return sharpPoints

        # now detect sharp points...
        theta = [(alpha[0] - alpha[1])]
        for k in range(1, len(trace.points) - 1):
            # use two different tests to detect sharp point...
            # 1) change in writing direction (as defined in [1])
            # 2) difference in writing direction angle between current point and
            #   and last sharp point higher than a threshold
            addPoint = False

            # for 1) calculate difference in writing direction between
            # current point and previous point
            if k < len(trace.points) - 2:
                theta.append( alpha[k] - alpha[k + 1] )

                if theta[k] != 0.0 and k > 0:
                    delta = theta[k] * theta[k - 1]

                    if delta <= 0.0 and theta[k - 1] != 0.0:
                        # direction at which the pen is moving has changed
                        addPoint = True

            # for 2) calculate the difference of angle between
            # current point and last sharp point
            phi = MathOps.angularDifference(alpha[sharpPoints[-1][0]], alpha[k])

            # circular angle....
            if phi > math.pi:
                phi = math.pi * 2 - phi

            if phi >= math.pi / 8:
                addPoint = True

            if addPoint:
                sharpPoints.append((k, trace.points[k]))

        # the last point is a sharp point
        sharpPoints.append((len(trace.points) - 1, trace.points[-1]))

        return sharpPoints

    # Resample the trace using splines (pre-processing)
    def splineResample(self, trace, subDivisions):
        sharp_points = trace.sharp_points

        new_points = []

        # check special case: Only one sharp_point
        if len(sharp_points) == 1:
            trace.points = [sharp_points[0][1]]
            return

        for i in range(len(sharp_points)):
            # add the sharp point
            new_points.append(sharp_points[i][1])

            if i < len(sharp_points) - 1:
                innerPoints = (sharp_points[i + 1][0] - sharp_points[i][0]) * subDivisions
                tStep = 1.0 / innerPoints

            # depending on the current range...
            if i == 0 or i == len(sharp_points) - 2:
                # between first and second sharp points...
                # or between the last two sharp points...
                # use linear interpolation...
                for k in range(1, innerPoints):
                    new_points.append(MathOps.lerp(sharp_points[i][1], sharp_points[i + 1][1], tStep * k))

            elif i < len(sharp_points) - 2:
                # in the middle of four sharp points... use Catmull-Rom
                for k in range(1, innerPoints):
                    point = MathOps.CatmullRom(sharp_points[i - 1][1], sharp_points[i][1],
                                               sharp_points[i + 1][1], sharp_points[i + 2][1], tStep * k)
                    new_points.append(point)

        # now replace
        trace.points = new_points

    # removes the hooks at the beginning and at the end (pre-processing)
    def removeHooks(self, trace):
        sharp_points = trace.sharp_points

        if len(sharp_points) >= 4:
            # remove hooks....

            # Check for hooks in the segment b (beginning)
            # and at the segment e (ending)

            #get the diagonal length....
            minX, maxX, minY, maxY  = trace.getBoundaries()
            ld = math.sqrt(math.pow(maxX - minX, 2) + math.pow(maxY - minY, 2))

            # at beginning
            betha_b0 = MathOps.slopeAngle(sharp_points[0][1], sharp_points[1][1])
            betha_b1 = MathOps.slopeAngle(sharp_points[1][1], sharp_points[2][1])
            lseg_b = trace.distance(sharp_points[0][0], sharp_points[1][0])
            lambda_b = MathOps.angularDifference(betha_b0, betha_b1)

            # At ending
            betha_e0 =  MathOps.slopeAngle(sharp_points[-1][1], sharp_points[-2][1])
            betha_e1 = MathOps.slopeAngle(sharp_points[-2][1], sharp_points[-3][1]);
            lseg_e = trace.distance(sharp_points[-1][0], sharp_points[-2][0])
            lambda_e = MathOps.angularDifference(betha_e0, betha_e1)

            if lambda_e > math.pi / 4 and lseg_e < 0.07 * ld:
                # remove sharp point at the end...
                del sharp_points[-1]

            if lambda_b > math.pi / 4 and lseg_b < 0.07 * ld:
                # remove sharp point at the beginning
                del sharp_points[0]

        return sharp_points

    # Add points where there are missing points (pre processing)
    def addMissingPoints(self, trace):
        # remove any duplicated point....
        self.removeDuplicatedPoints(trace)

        # calculate Le (average segment length) as defined in [1]
        Le = 0
        for i in range(len(trace.points) - 1):
            Le += trace.distance( i, i + 1)
        Le /= len(trace.points)

        # the distance used to insert points...
        d = 0.95 * Le
        i = 0
        while i < len(trace.points) - 1:
            # search point to interpolate ...
            n = 1
            length = trace.distance(i, i + n)
            sum = 0

            while sum + length < d and i + n + 1 < len(trace.points):
                n += 1
                sum += length
                length = trace.distance(i + n - 1, i + n)

            diff = d - sum

            # insert a point between i + n- 1 and i +n at distance diff
            # use linear interpolation...
            w2 = diff / length


            if w2 < 1.0:
                xp = trace.points[i + n - 1][0] * (1- w2) + trace.points[i + n][0] * w2
                yp = trace.points[i + n - 1][1] * (1- w2) + trace.points[i + n][1] * w2

                #check for collision with next point...
                insert = True

                if i + n < len(trace.points):
                    if (xp, yp) == trace.points[i + n]:
                        # weird case where a point after interpolated falls of the same
                        # coordinates as next point, don't insert it
                        insert = False

                if insert:
                    trace.points.insert(i + n, (xp, yp))

            else:
                # at the end, no point added but erase the one at the end
                #n += 1
                pass

            # now erase points from i + 1 .. f + n - 1
            toErase = n - 1
            for j in range(toErase):
                del trace.points[i + 1]

            i += 1

    # smooths the curve (pre-processing)
    def apply_catmull_Smoothing(self, trace):
        # Detect sharp points and extract them....
        trace.sharp_points = self.getSharpPoints(trace)
        #print (trace.sharp_points)
        # Remove hooks using sharp points information
        self.removeHooks(trace)

        # Finally re-sample
        self.splineResample(trace, 2)

        # bounding box might have changed...
        trace.bounding_box = None

    def apply_linear_smoothing(self, clean_trace):
        points = clean_trace.points
        if len(points)>2:
                new_points = []
                new_points.append(points[0])
                for i in range(len(points)-2):
                        pre_point = points[i]
                        current_point = points[i+1]
                        next_point = points[i+2]
                        smooth_point = (np.array(pre_point) + np.array(current_point) + np.array(next_point))/3.0
                        new_points.append(tuple(smooth_point))
                new_points.append(points[len(points)-1])
        else:
                new_points = points
        #create new smooth stroke with linear interpolation
        clean_trace.points =new_points


    def linear_resampling(self, smooth_symbol_trace):
        # Detect sharp points and extract them....
        smooth_symbol_trace.sharp_points = self.getSharpPoints(smooth_symbol_trace)
        points = smooth_symbol_trace.points
        if len(points)>1:
            new_points = []
            dl = 3.125e-1
            ## line rendering
            for i in range(len(points)-1):
                current_point = points[i]
                next_point = points[i+1]
                dx = next_point[0] - current_point[0]
                dy = next_point[1] - current_point[1]
                l = 0.0
                while(l<1.0):
                    x = current_point[0] + dx*l
                    y = current_point[1] + dy*l
                    l+=dl
                    new_points.append((x, y))
            new_points.append(points[len(points)-1])
        else:
            new_points = points

        final_new_points = []

        for i in range(len(new_points)):
            final_new_points.append((round(new_points[i][0]), round(new_points[i][1])))
        smooth_symbol_trace.points =  final_new_points


    def fixTrace(self, trace):
        # 1) first step of pre processing: Remove duplicated points
        self.removeDuplicatedPoints(trace)
        #print('self.debug_raw',self.debug_raw)

        # output raw data
        if self.debug_raw:
            #print('raw traces are written')
            trace.save_to_file(self.debug_raw + str(trace.id) + '.txt')

        # Add points to the trace...
        self.addMissingPoints(trace)

        # output data after adding points 
        if self.debug_added:
            trace.save_to_file(self.debug_added + str(trace.id) + '.txt')

        if self.smoothening_type.lower() == "catmull":
            # Apply re-sampling to the trace...
            self.apply_catmull_Smoothing(trace)
        else:
            # apply linear smoothening to the trace points
            self.apply_linear_smoothing(trace)
            self.linear_resampling(trace)

        # it should not ... but .....
        if trace.hasDuplicatedPoints():
            # ...remove them! ....
            self.removeDuplicatedPoints(trace)

        # output data after smoothing
        if self.debug_smooth:
            trace.save_to_file(self.debug_smooth + str(trace.id) + '.txt')

    def normalizeExpression(self, expression):
        for key,comp in expression.componentMap.items():
            expression.componentMap[key] = self.normalizeTraceset(comp)
        expression.normalized = True
        return expression

    def normalizeTraceset(self, traceset):
        # first, get the bounding box of the whole symbol...
        current_box = traceset.getSquaredBoundingBox()
        #print ('before',traceset.traces)
        # relocate and re-scale each trace, to be in the new box [-1, 1],[-1,1]
        new_box = [-1, 1, -1, 1]

        traces = []
        for trace in traceset.traces:
            trace.relocatePoints(current_box, new_box)
            traces.append(trace)
        traceset.traces = traces
        #print ('after',traceset.traces)
        return traceset

    def applyPreprocessing(self, expression):
        #self.normalizeEntireEquation(expression)
        for key,comp in expression.componentMap.items():
            for trace in comp.traces:
                self.fixTrace(trace)
            if self.normalize:
                #expression.setUnnormalized() #MM:before normalizing the traces keep an unnormalized version
                expression.componentMap[key] = self.normalizeTraceset(comp)
                expression.normalized = True
            else:
                expression.normalized = False
        #print('normalize traces',expression.normalized)                
        return expression


    @staticmethod
    def normalizeExpressions(expressions, config, n_workers=4):
        preprocessor = Preprocessor(config)
        with ProcessPoolExecutor(max_workers=n_workers) as executor:
            #executor.map(preprocessor.applyPreprocessing, list(expressions.values()))
            mapper = executor.map
            for i, exp in enumerate(mapper(preprocessor.normalizeExpression, list(expressions.values()))):
                expressions[exp.id] = exp
        return expressions


    @staticmethod
    def preprocessExpressions(expressions, config):
        preprocessor = Preprocessor(config)

        for math_expression in expressions:
            preprocessor.applyPreprocessing(math_expression)

    @staticmethod
    def preprocessExpressionsParallel(expressions, config, n_workers=4):
        preprocessor = Preprocessor(config)
        with ProcessPoolExecutor(max_workers=n_workers) as executor:
            #executor.map(preprocessor.applyPreprocessing, list(expressions.values()))
            mapper = executor.map
            for i, exp in enumerate(mapper(preprocessor.applyPreprocessing, list(expressions.values()))):
                expressions[exp.id] = exp
                #print(exp)
                print(" Preprocessing: " + str(((i+1)*100)//len(expressions)) + "%", end="\r")
            print()
                        
        '''
        print('***************print expressions after preprocessing****************')
        expObj=list(expressions.values())
        exp=expObj[1]  #MM: choosing one exp object
        comp=exp.componentMap['0']
        print ('preprocessed points')
        print(comp.traces)

        count=0
        for trace in comp.traces:
            #print(trace) #MM: this is exactly the comp.traces that I printed, but i cannot get sharp_points from it other that in a loop!!! WTF
            print('sharp points')
            print(trace.sharp_points)
            count=count+1
            print('number of traces in comp.traces:')
            print(count)

        '''
        return expressions


