import os
import sys
import fnmatch
import xml.etree.ElementTree as ET

from concurrent.futures import ProcessPoolExecutor

from recognizer.symbols.math_symbol_trace import MathSymbolTrace
from recognizer.symbols.math_trace_set import MathSymbolTraceSet
from recognizer.symbols.math_relation import Relation
from recognizer.symbols.math_expression import MathExpression
from recognizer.preprocessing.trace_smoother import TraceSmoother
from Cython.Plex.Regexps import BOL
from recognizer.graph_representation.Graph_construction import GraphConstruction, EDGE_ATTRIBUTES
from recognizer.preprocessing.lg_loader import LGLoader
from recognizer.visualization.visualize_expression import Visualization

class INKMLLoader:
    INKML_NAMESPACE = '{http://www.w3.org/2003/InkML}'
    PREPROCESS_TYPE = "CATMULL"
    GRAPH_REPRESENTATION="LINE_OF_SIGHT"
    DO_VISUALIZE = False


    @staticmethod
    def set_Extraction_Parameters(config):
        '''
        Graph representation and other preprocessing parameters
        :param config:
        :return:
        '''
        if config.contains("GRAPH_REPRESENTATION"):
            INKMLLoader.GRAPH_REPRESENTATION=(config.get("GRAPH_REPRESENTATION"))
        elif config.contains("INITIAL_GRAPH_TYPE"):
            INKMLLoader.GRAPH_REPRESENTATION=config.get("INITIAL_GRAPH_TYPE")
        else:
            INKMLLoader.GRAPH_REPRESENTATION = "LINE_OF_SIGHT"

    def instance_process_INKML_file(self, data):
        return INKMLLoader.process_INKML_file(data)

    def instance_process_INKML_file_as_Graph(self, data):
        return INKMLLoader.process_INKML_file_as_Graph(data)

    @staticmethod
    def path_INKML_filenames(path):
        complete_list = os.listdir(path)
        filtered_list = []
        for local_name in complete_list:
            full_name = path + "/" + local_name
            if os.path.isdir(full_name):
                child_filenames = INKMLLoader.path_INKML_filenames(full_name)
                for child_filename in child_filenames:
                    filtered_list.append(local_name + "/" + child_filename)
            else:
                if fnmatch.fnmatch(local_name, '*.inkml'):
                    filtered_list.append(full_name)

        return filtered_list

    @staticmethod
    def path_INKML_LG_filenames(inkml_path, lg_path):
        #the recognizer doesn't only rely on INKML files any more and hence we have to get the list of [inkml_file, lg_file]
        inkml_complete_list = os.listdir(inkml_path)

        inkml_file_list = []
        lg_file_list=[]
        for local_name in inkml_complete_list:
            inkml_full_name = inkml_path + "/" + local_name
            #get the corresponding lg file name
            (file_name_prefix, partitioner, file_extension) = local_name.rpartition(".")
            lg_full_name = lg_path+"/"+file_name_prefix+".lg"

            if fnmatch.fnmatch(local_name, '*.inkml'):
                    inkml_file_list.append(inkml_full_name)
                    if(os.path.isfile(lg_full_name)):
                        lg_file_list.append(lg_full_name)
                    else:
                        raise Exception("lg file for this inkml not found..."+inkml_full_name)


        return (inkml_file_list, lg_file_list)

    @staticmethod
    def load_label_replacement(filename):
        # load file
        replacement_labels_file = open(filename, "r")
        lines = replacement_labels_file.readlines()
        replacement_labels_file.close()

        replacement_labels = {}
        for line in lines:
            parts = line.strip().split("\t")
            if len(parts) == 2:
                replacement_labels[parts[0]] = parts[1]

        return replacement_labels
    
    @staticmethod
    def set_proprocess_type(preprocess_type):
        INKMLLoader.PREPROCESS_TYPE=preprocess_type

    @staticmethod
    def set_graph_representation_type(graph_type):
        INKMLLoader.GRAPH_REPRESENTATION = graph_type

    @staticmethod
    def set_visualization_parameters(visualize_expression, visualization_plot_path):
        INKMLLoader.DO_VISUALIZE = visualize_expression
        INKMLLoader.VISUALS_PLOT_PATH = visualization_plot_path

    @staticmethod
    def load_INKML_annotations(root):
        all_annotations = {}
        for annotation in root.findall(INKMLLoader.INKML_NAMESPACE + 'annotation'):
            try:
                key = annotation.attrib["type"]
                value = annotation.text

                all_annotations[key] = value
            except:
                # ignore annotation
                continue

        return all_annotations

    @staticmethod
    def load_INKML_traces(root, debug_raw_prefix=None, debug_added_prefix=None, debug_smooth_prefix=None):

        # extract all the traces first...
        trace_objects = {}
        for trace in root.findall(INKMLLoader.INKML_NAMESPACE + 'trace'):
            # load
            object_trace = MathSymbolTrace.from_INKML_trace(trace)

            # apply general trace pre processing...
            #TraceSmoother.applyPreprocessing(object_trace, debug_raw_prefix, debug_added_prefix, debug_smooth_prefix, INKMLLoader.PREPROCESS_TYPE)
            # add to the dictionary...
            trace_objects[object_trace.id] = object_trace

        return trace_objects

    @staticmethod
    def load_INKML_primitives(root, trace_objects, ground_truth_available, active_classes=None, debug_normalization_prefix=None):
        # read an inkml file,
        # return the symbols as stroke-set objects
        #           traces as stroke-set objects
        #           maps that can predict relationship between trace-id-> symbol, trace-id->trace (object), symbol_id -> [list of trace_ids]
        #these mappings accelarate construction of the layout trees instead of iterating through the list
        groups_root = root.find(INKMLLoader.INKML_NAMESPACE + 'traceGroup')
        trace_groups = groups_root.findall(INKMLLoader.INKML_NAMESPACE + 'traceGroup')
        trace_id_symbol_map = {}
        trace_id_stroke_map = {}
        symbol_id_symbol_map = {}
        segmentation_map = {}

        symbols = []
        traces =[]
        avg_width = 0.0
        avg_height = 0.0
        for group in trace_groups:
            if ground_truth_available:
                # search for class label...
                symbol_class = group.find(INKMLLoader.INKML_NAMESPACE + 'annotation').text

                if symbol_class is None:
                    symbol_class = "junk"

                if active_classes is not None and not symbol_class in active_classes:
                    # skip this symbol since it does not belong to the current set of classes
                    continue

            else:
                # unknown class and id label
                symbol_class = '{Unknown}'
            # search for id attribute...
            symbol_id = 0
            for id_att_name in group.attrib:
                if id_att_name[-2:] == "id":
                    try:
                        symbol_id = int(group.attrib[id_att_name])
                    except:
                        # could not convert to int, try spliting...
                        symbol_id = str(
                            group.attrib[id_att_name].split(":")[0])

            # link with corresponding traces...
            group_traces = group.findall(INKMLLoader.INKML_NAMESPACE + 'traceView')

            symbol_trace_list = []
            symbol_trace_id_list = []

            #this is the part of processing primitives and writing the desired information
            #here every trace is written as a stroke_set object
            #every symbol (collection of trace) is written as stroke_Set object

            for trace in group_traces:
                #for every trace, create a new math symbol object and append in the symbols-list
                object_trace = trace_objects[int(trace.attrib["traceDataRef"])]
                trace_id=str(trace.attrib["traceDataRef"])
                #create a trace-set object with current trace alone)
                trace_ss = MathSymbolTraceSet(trace_id, [object_trace], symbol_class)
                trace_ss.setTraceList([trace_id])
                traces.append(trace_ss)
                #reference- map the symbol object to trace_id_symbol map
                trace_id_stroke_map[trace_id] = trace_ss
                #append the details to list - to use for symbol object
                symbol_trace_id_list.append(trace_id)
                symbol_trace_list.append(object_trace)


            #create a new symbol object with list of traces
            new_symbol = MathSymbolTraceSet(symbol_id, symbol_trace_list, symbol_class)
            new_symbol.setTraceList(symbol_trace_id_list)
            symbols.append(new_symbol)
            symbol_id_symbol_map[symbol_id] = new_symbol
            #segmentation map finds the list of traces assosiated with a symbol-id
            #this map speeds up the lookup when processing the expression-layout graph
            segmentation_map[symbol_id] = symbol_trace_id_list

            # iterate over the trace_ids to again map the symbol to the trace_ids
            for trace_id in symbol_trace_id_list:
                trace_id_symbol_map[trace_id] = new_symbol

            if debug_normalization_prefix is not None:
                # output data after relocated
                for trace in group_traces:
                    object_trace = trace_objects[int(trace.attrib["traceDataRef"])]

                    file = open(debug_normalization_prefix + str(object_trace.id) + '.txt', 'w')
                    file.write(str(object_trace))
                    file.close()

        return symbols, traces, trace_id_symbol_map, trace_id_stroke_map, symbol_id_symbol_map, segmentation_map


    @staticmethod
    def load_INKML_file(filename, lg_file_path, ground_truth_available, active_classes=None):
        #extract the layout graph from the inkml-files and the desired mappings
        #this returns a math expression object
        # first load the tree...
        tree = ET.parse(filename)
        root = tree.getroot()
        
        traces_objects = INKMLLoader.load_INKML_traces(root)
        symbols, traces, trace_id_symbol_map, trace_id_stroke_map, symbol_id_symbol_map, segmentation_map = INKMLLoader.load_INKML_primitives(root, traces_objects, ground_truth_available, active_classes)


        #stroke level relations - read from lg file
        stroke_level_relations = LGLoader.get_Relations(lg_file_path)
        stroke_truth_map=LGLoader.get_trace_truth(lg_file_path)

        symbol_level_relations = []
        trace_level_relation_map={}
        #find the symbol_level relations from the stroke_level relations and the stroke-> symbol mapping
        for relation in stroke_level_relations:
            #for a stroke_id, find the corresponding symbol-id and create a relation
            parent_stroke_id, child_stroke_id, layout_relation = relation
            trace_level_relation_map[(parent_stroke_id, child_stroke_id)] = layout_relation
            parent_symbol_id = trace_id_symbol_map[parent_stroke_id].getId()
            child_symbol_id = trace_id_symbol_map[child_stroke_id].getId()
            symbol_level_relations.append([parent_symbol_id, child_symbol_id, layout_relation])

        '''gt_symbol_labels={}
        for trace_id, symbol_object in trace_id_symbol_map.items():
            gt_symbol_labels[trace_id] = trace_id_symbol_map[trace_id].truth'''

        #construct the layout graph from the relations
        #initalize the math expression instance
        expression = MathExpression(filename, componentMap=trace_id_stroke_map, gt_symbolLabels=stroke_truth_map, gt_relationLabels=trace_level_relation_map, gt_segmentations=segmentation_map, source=filename)
        #stroke_layout_graph = GraphConstruction.get_line_of_sight_graph(expression)
        #expression.setLayoutGraph(stroke_layout_graph)
        avg_width = expression.getSizes()[:,0].mean()/len(traces)
        avg_height = expression.getSizes()[:,1].mean()/len(traces)

        # set the formula information in every symbol, to get contextual
        # features later
        for symbol in symbols:
            symbol.set_expression(expression)
            symbol.set_file_name(filename)
            symbol.setSizeRatio(avg_width, avg_height)
            #symbol.normalize()

        for trace in traces:
            trace.set_expression(expression)
            trace.set_file_name(filename)
            trace.setSizeRatio(avg_width, avg_height)
        #print ('*********************',vars(expression).keys())
        #print ('*********************',expression.expressionGraph)
        return expression


    @staticmethod
    def process_INKML_file_as_Graph(data):
        #get the expression objects, tracks the status of failed to load files and success files

        file_path, ground_truth_available, active_classes = data
        inkml_file_path, lg_file_path = file_path
        try:
            expression = INKMLLoader.load_INKML_file(inkml_file_path, lg_file_path, ground_truth_available, active_classes)
            success = (True, None)
            #print('MM:loading them successfully')
        except Exception as e:
            raise e
            success = (False, e)
            return file_path, success, None

        return file_path, success, expression



    @staticmethod
    def get_Expression_Objects(inkml_path, lg_path, workers=3, ground_truth_available=True, active_classes=None):
        '''
        Given a directory of Handwritten files, it returns the list of math expression objects
        :param inkml_path: path of input inkml files to get trace information
        :param lg_path: to know the ground truth information - how traces are related
        :param workers: number of threads to work on this
        :param ground_truth_available: if the inkml file has groundtruth label in it. This
        :param active_classes: classes to look-for in the inkml file
        :return: list of expression instances
        '''
        print(inkml_path, lg_path)
        (complete_list_Inkml, complete_list_Lg) = INKMLLoader.path_INKML_LG_filenames(inkml_path, lg_path)

        #Initialize the data holders
        raw_data, raw_labels, valid_files, error_files = [], [], [], []
        dir_data = []
        for inkml_file, lg_file in zip(complete_list_Inkml, complete_list_Lg):
            file_path = (inkml_file, lg_file)
            file_data = (file_path, ground_truth_available, active_classes)
            dir_data.append(file_data)

        # read in parallel every file in the path specified...
        empty_string = " " * 80
        expression_objects = []
        file_name_expression_map = {}

        #create N workers
        with ProcessPoolExecutor(max_workers=workers) as executor:
            #set loading function based on python version
            if sys.version_info.major == 3 and sys.version_info.minor >= 5:
                #Set the loading function
                mapper = executor.map
                # check of the processing has to be graph based
                loading_function = INKMLLoader.process_INKML_file_as_Graph
            else:
                # avoid issues in versions older than python 3.5
                mapper = executor.map
                tempo_instance = INKMLLoader()
                # if the dataset extraction is for classification, then consider the extraction as symbol by symbol
                loading_function = tempo_instance.instance_process_INKML_file_as_Graph

            #for every file process and get the expression objects
            for i, (file_path, success, expression_graph) in enumerate(mapper(INKMLLoader.process_INKML_file_as_Graph, dir_data)):
                advance = float(i) / len(complete_list_Inkml)
                #print(empty_string, end="\r")
                inkml_path, lg_path = file_path
                #print(("Graph Representation of expression => {:.2%} => " + inkml_path).format(advance), end="\r")
                expression_objects.append(expression_graph)
                file_name_expression_map[expression_graph.id] = expression_graph

                loading_success, error_desc = success
                if not loading_success:
                    print("Failed processing: " + inkml_path)
                    print(error_desc)
                    error_files.append(file_path)
                else:
                    valid_files.append(file_path)

        if INKMLLoader.DO_VISUALIZE:
            for expression in expression_objects:
                visualize = Visualization()
                plotPath = INKMLLoader.VISUALS_PLOT_PATH + "//" + expression.source.replace(".inkml", "")
                visualize.visualize_expression_graph(expression, plotPath)

        return expression_objects, file_name_expression_map, valid_files, error_files

