from recognizer.recognition.general_expert import GeneralExpert
from recognizer.recognition.Filtered_parser import FilteredParser
import numpy as np
from matplotlib import pyplot as plt

class FilterThresholdParser(FilteredParser):

    def __init__(self, type, dataset, config, scaler=None, probabilistic=False):
        super(FilterThresholdParser, self).__init__(type, dataset, config, scaler=scaler, probabilistic=probabilistic)
        self.set_threshold(dataset)


    def find_optimal_threshold(self, truth_confidence, actual_labels):
        '''
        find threshold value in truth_confidence, above the confidence all the labels are true
        :param truth_confidence:
        :param actual_labels:
        :return:
        '''
        possible_thresholds = np.linspace(1.0, 0.0, num=101)
        #initailize the miss count as , all the True values (all relations getting missed
        total_relations = actual_labels.sum()

        for threshold in possible_thresholds:
            #truth confidence more than the threshold are recovered relations
            recovered = actual_labels[truth_confidence >= threshold].sum()
            #if 99% of relations are recoved then set that value as threshold
            if recovered/total_relations >= 0.99:
                return threshold
        return threshold


    def set_threshold(self, feature_dataset):
        # get truth confidence

        predicted_labels, confidence = self.predictClassNamesAndConfidence(feature_dataset)
        confidence[~predicted_labels] = 1 - confidence[~predicted_labels]
        #the truth confidence value above which all values are true
        true_index = feature_dataset.class_mapping[True]
        actual_labels = (feature_dataset.labels == true_index)
        self.threshold = self.find_optimal_threshold(confidence, actual_labels[:,0])


    def predictClassNames(self, feature_dataset):
        '''
        predict a different class label depending on the threshold chosen
        :param feature_dataset:
        :return:
        '''
        predicted_labels, confidence = self.predictClassNamesAndConfidence(feature_dataset)

        #from confidence value, find the truth confidence of
        confidence[~predicted_labels] = 1 - confidence[~predicted_labels]

        print("The chosen threshold for this dataset is", self.threshold)
        #if the truth confidence is above the threshold, convert the prediction to True (include the edges in graph)
        predicted_labels[confidence > self.threshold] = True
        return predicted_labels