from recognizer.recognition.general_expert import GeneralExpert
from recognizer.evaluation.eval_ops import EvalOps
import numpy as np
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES
from recognizer.graph_representation.Graph_construction import GraphConstruction



class SpatialParser(GeneralExpert):
    '''
    Specialize the output prediction and operate function to act as a spatial parser expert
    '''

    def __init__(self, type, dataset, config, scaler=None, probabilistic=False):
        super(SpatialParser, self).__init__(type, dataset, config, scaler=scaler, probabilistic=probabilistic)


    def predictClassNamesAndConfidence(self, eval_dataset):
        '''
        ignore a class_value(No-Relation) and predict the best possible class labels from the remaining classes
        :param classifier:
        :param test_dataset:
        :return:
        '''
        predicted_output, confidence, evaluation_metrics = EvalOps.classifier_per_class_accuracy_exclude(self.trained_classifier.trained_classifier,eval_dataset, 'NORelation', get_output_data=True, normalize_confidence=False)

        return np.array(predicted_output), np.array(confidence)


    def operate(self, expression_map, dataset):
        '''
        for item in the dataset, updates the corresponding the layout graph
        :param expressions: expression_file_names mapped to expression objects
        :param dataset: the source, feature, ground_truth label
        '''
        predicted_labels, confidences = self.predictClassNamesAndConfidence(dataset)
        self.update_expression_graph(dataset,  predicted_labels, confidences, expression_map, extractMst=True)


    def update_expression_graph(self, dataset, predicted_labels, confidences, expression_map, extractMst=True):
        '''
         for item in the dataset, updates the corresponding the layout graph
         :param expression_map: expression_file_names mapped to expression objects
         :param dataset: the source, feature, ground_truth label
        '''
        length_dataset = len(dataset.sources)
        entire_data = zip(dataset.sources, predicted_labels, confidences)
        for i, data in enumerate(entire_data):
            # [user update console message] compute the amount of dataset processed
            advance = float(i) / length_dataset
            # data is one edge record in the entire data-set
            # every edge is represented by sourceinformation(Expression_file name, parent-edge-id, child-edge-id
            #  label predicted for the edge
            source, predicted_label, confidence = data
            expr = expression_map[str(source[0])]
            layout_graph = expr.getLayoutGraph()
            parent_id = str(source[1])
            child_id = str(source[2])
            layout_graph.edge[parent_id][child_id][EDGE_ATTRIBUTES.PREDICTED_LAYOUT] = predicted_label
            layout_graph.edge[parent_id][child_id]['weight'] = confidence
            print(("Graph Representation update => {:.2%}").format(advance), end="\r")

        if extractMst:
            for expr_name, expr in expression_map.items():
                layout_tree = GraphConstruction.get_Maximum_Spanning_Tree(expr.getLayoutGraph())
                GraphConstruction.set_graph_attributes(expr.getLayoutGraph(), layout_tree)
                expr.setLayoutGraph(layout_tree)

        print("\n\nUpdated Labels for all edges in the graph")