from recognizer.recognition.general_expert import GeneralExpert
import numpy as np
from recognizer.training.trainer import ClassifierTrainer
from recognizer.data.feature_dataset import FeatureDataset

class FilteredParser(GeneralExpert):

    def __init__(self, type, dataset, config, scaler=None, probabilistic=False):
        super(FilteredParser, self).__init__(type, dataset, config, scaler=scaler, probabilistic=probabilistic)

    def trainClassifier(self, feature_dataset, config):
        '''
        Masks the output as False (No relation) or True (other layout relations)
        :param feature_dataset: training dataset
        :param config:
        :return: helper object of type 'ClassificationTrainer'
        '''
        self.mask = {}
        self.mask['NoRelation'] = False
        self.mask['default_label'] = True

        self.mask_data(feature_dataset)
        classification_helper = ClassifierTrainer.train_classifier(feature_dataset, config)
        return classification_helper

    def mask_data(self, feature_dataset):
        masked_labels, updated_label_mapping, updated_class_mapping = FeatureDataset.createNewMaskMapping(feature_dataset, self.mask)
        feature_dataset.labels = masked_labels
        feature_dataset.class_mapping = updated_class_mapping
        feature_dataset.label_mapping = updated_label_mapping


    def filter_layout_graph_edges(self, dataset, edges_prediction, expression_map):
        '''
        for item in the dataset, removes/adds the ege in the expression graph based on prediction
        :param expression_map: expression_file_names mapped to expression objects
        :param dataset: the source, feature, ground_truth label
                '''
        length_dataset = len(dataset.sources)
        entire_data = zip(dataset.sources, edges_prediction)
        for i, data in enumerate(entire_data):
            # [user update console message] compute the amount of dataset processed
            advance = float(i) / length_dataset
            # data is one edge record in the entire data-set
            # every edge is represented by source information(Expression_file name, parent-edge-id, child-edge-id
            #  label predicted for the edge
            source, prediction = data

            layout_graph = expression_map[str(source[0])].getLayoutGraph()
            parent_id, child_id = source[1], source[2]
            if not prediction:
                layout_graph.remove_edge(parent_id,child_id)
            print(("Graph Filtering update => {:.2%}").format(advance), end="\r")

        print("\n\nFiltered edges in the graph")



    def operate(self, expression_map, dataset, *args):
        '''
        predict chance of relationship of every edge in the alyout graph
        remove the edges with least chances
        predict spatial relationship for item in the dataset, updates the corresponding the layout graph
        :param expressions: expression_file_names mapped to expression objects
        :param dataset: the source, feature, ground_truth label
        '''
        predicted_labels = self.predictClassNames(dataset)
        self.filter_layout_graph_edges(dataset, predicted_labels, expression_map)