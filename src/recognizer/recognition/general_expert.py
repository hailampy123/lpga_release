import numpy as np
from recognizer.training.trainer import ClassifierTrainer
from recognizer.evaluation.eval_ops import EvalOps

class GeneralExpert:

    def __init__(self, type, dataset, config, scaler=None, probabilistic=False):
        self.type = type
        self.classification_helper = self.trainClassifier(dataset, config)
        self.trained_classifier = self.classification_helper.trained_classifier
        self.classes_list = dataset.label_mapping
        self.classes_dict = dataset.class_mapping
        self.config = config
        self.scaler = scaler
        self.probabilistic = probabilistic

    def trainClassifier(self, feature_dataset, config):
        '''
        trains a classifier (RandomForest/SVM based on Config) based on the training dataset
        :param feature_dataset: training dataset
        :param config: for getting classifier parameters
        :return: classifier
        '''
        classification_helper= ClassifierTrainer.train_classifier(feature_dataset, config)
        return classification_helper

    def get_raw_classes(self):
        '''
        get the classes of the dataset passed
        :return: classes
        '''
        return self.trained_classifier.classes_

    '''
    Class Names
    '''
    def predictClassNames(self, feature_dataset):
        '''
        for the given dataset returns the list of predicted class values
        :param feature_dataset: testing dataset
        :return: class-labels for every testing dataset instance
        '''
        predicted = self.trained_classifier.predict(feature_dataset.data)
        return [self.classes_list[p] for p in predicted]

    def predictClassName(self, feature_vector):
        '''
        predict the class label for the given feature vector
        :param feature_vector:
        :return:
        '''
        predicted = self.trained_classifier.predict(feature_vector)
        return self.classes_list[predicted[0]]

    '''
    Probabilities
    '''

    def predictProbabilities(self, feature_dataset):
        '''
        for the given dataset returns list of confidence/probability of each classes
        :param feature_dataset:
        :return: list of confidence score for every score
        '''
        predicted = self.trained_classifier.predict_proba(feature_dataset.data)

        all_confidences = []
        n_samples = predicted.shape[0]
        for idx in range(n_samples):
            scores = sorted([(predicted[idx, k], k) for k in range(predicted.shape[1])], reverse=True)
            tempo_classes = self.trained_classifier.classes_
            n_classes = len(tempo_classes)
            confidences = [(self.classes_list[tempo_classes[scores[k][1]]], scores[k][0]) for k in range(n_classes)]
            all_confidences.append(confidences)
        return all_confidences


    def predictConfidence(self, feature_vector):
        '''
        for a single feature vector predict the confidence for every class
        :param feature_vector:
        :return: list of confidence score for every
        '''
        try:
            predicted = self.classification_helper.predict_proba(feature_vector)
        except:
            raise Exception("Classifier was not trained as probabilistic classifier")

        scores = sorted([(predicted[0, k], k) for k in range(predicted.shape[1])], reverse=True)

        tempo_classes = self.trained_classifier.classes_
        n_classes = len(tempo_classes)

        confidences = [(self.classes_list[tempo_classes[scores[k][1]]], scores[k][0]) for k in range(n_classes)]

        return confidences


    def predictClassNamesAndConfidence(self, eval_dataset):
        '''
        predict class-Labels and their confidence for the given dataset
        :param eval_dataset:
        :return:
        '''
        predicted_output, confidence, evaluation_metrics = EvalOps.classifier_prob_per_class_accuracy(self.classification_helper,
                                                                                                      eval_dataset,
                                                                                                      get_output_data=True)
        return np.array(predicted_output), np.array(confidence)



    def writeRelationProbabilities(self, expressions, feature_dataset):
        predicted_probs = self.predictProbabilities(feature_dataset)

        for i, prob in enumerate(predicted_probs):
            classes = np.array(predicted_probs[i])[:,0]
            probs = np.array(predicted_probs[i])[:,1]
            exprId, pid, cid = feature_dataset.sources[i]
            graph = expressions[exprId].expressionGraph
            graph.edge[pid][cid]['classes'] = classes
            graph.edge[pid][cid]['scores'] = probs


    def mask_data(self, feature_dataset):
        feature_dataset.data = feature_dataset.data
        feature_dataset.labels = feature_dataset.labels


    def operate(self, expressions, feature_dataset, *args):
        pass
