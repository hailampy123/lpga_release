
import math
import numpy as np
from scipy.spatial import distance

class MathOps:
    # Euclidean distance
    @staticmethod
    def euclideanDistance(x1, y1, x2, y2):
        return math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)

    # Returns the angle of the slope of the line that connects p1 and p2
    @staticmethod
    def slopeAngle(p1, p2):
        x = p2[0] - p1[0]
        y = p2[1] - p1[1]

        return math.atan2(y, x)

    # Find the absolute minimum difference between two angles
    @staticmethod
    def angularDifference(alpha, beta):
        diff = abs(alpha - beta)

        # circular ....
        while diff > 2 * math.pi:
            diff -= 2 * math.pi

        # now, the value should be positive, but it should not be above PI
        if diff > math.pi:
            # if above PI, then the minimum difference is 2 PI - diff
            diff = math.pi * 2 - diff

        return diff

    # linear interpolation
    @staticmethod
    def lerp(p1, p2, t):
        x = p1[0] * (1- t) + p2[0] * t
        y = p1[1] * (1 -t) + p2[1] * t

        return (x, y)

    # Spline interpolation using Catmull-Rom method
    @staticmethod
    def CatmullRom(p1, p2, p3, p4, t):
        x1, y1 = p1
        x2, y2 = p2
        x3, y3 = p3
        x4, y4 = p4

        xt =  0.5 * ((-x1 + 3*x2 -3*x3 + x4)*t*t*t
               + (2*x1 -5*x2 + 4*x3 - x4)*t*t
               + (-x1+x3)*t
               + 2*x2)
        yt =  0.5 * ((-y1 + 3*y2 -3*y3 + y4)*t*t*t
               + (2*y1 -5*y2 + 4*y3 - y4)*t*t
               + (-y1+y3)*t
               + 2*y2)

        return xt, yt

    @staticmethod
    def get_1d_gaussian_kernel(size, sigma):
        result = np.zeros(size)

        mid = int(size / 2)
        for i in range(-mid, mid + (size % 2)):
            result[i + mid] = (1 / (sigma * np.sqrt(2 * np.pi)))*(1 / (np.exp((i**2) / (2 * sigma**2))))

        # normalize
        total = result.sum()
        result /= total

        return result

    @staticmethod
    def get_parzen_density_vector(sample_points, current_points, variance, isNormalized=True, parzen_normalization_method="ENTIRE_POINTS"):
        '''
            get the features for the current points, w.r.t sample points
            radius : factor for standard deviation of parzen density
            normalization method :
                How to Normalize the parzen features : Point by point or all points together
        '''
        if parzen_normalization_method != "ENTIRE_POINTS" and parzen_normalization_method != "POINT_BY_POINT":
            raise Exception("Undefined Normalization of points")

        sample_points_shape = sample_points.shape
        if(current_points.size==0) or (sample_points.size ==0) or  variance == 0:
            return np.zeros(sample_points_shape[0])

        #Calculates the point wise distance from every point given vs every sample point : forms a matrix of distance
        dist_array = np.exp(-(distance.cdist(sample_points, current_points) **2)/ (2 * variance)) / math.sqrt(2 * math.pi * variance)

        #get the point-wise sum
        if (parzen_normalization_method == "POINT_BY_POINT"):
            #sum up contributions from indi points
            points_contributions = dist_array.sum(axis=0)
            #makes the contributions from every point to be one
            dist_array = dist_array/points_contributions

        #sumup the contributions to every sample point
        parzen_density=dist_array.sum(axis=1)

        if isNormalized:
            norm_factor = parzen_density.sum()
            return parzen_density/norm_factor if norm_factor > 0.0 else parzen_density

        if parzen_density.size != sample_points_shape[0]:
            raise Exception("Something wrong in parzen density calculation")
        return parzen_density