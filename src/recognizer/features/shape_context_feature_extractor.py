'''
@author : Lakshmi Ravi
@author : Chinmay Jain

This library to extract shape context features for a given symbol in an expression
'''

import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.spatial import distance
from scipy import stats
from recognizer.util.math_ops import MathOps
import os

class ShapeContextFeatureExtractor:
    def __init__(self, config):
        self.config = config

        # Override parameters
        self.circle_count_override = config.get_int("SHAPE_CONTEXT_CIRCLE_COUNT", None)
        self.sectors_count_override = config.get_int("SHAPE_CONTEXT_SECTOR_COUNT", None)
        self.standard_dev_factor_override = config.get_float("PARZEN_STD_DEV_FACTOR", None)
        self.context_fraction = config.get_float("EXPRESSION_SHAPE_CONTEXT_FRACTION",0.0)

        # Default parameters for SYMBOL shape context feature extraction
        self.scc_radius_method = "SYMBOL_FARTHEST_PT"
        self.radius_restriction_factor = 1.0
        self.required_features = "SYMBOL_ONLY"

        self.debug_mode = config.get_bool("SHAPE_CONTEXT_DEBUG_MODE")
        if self.debug_mode:
            self.intensity_type = config.get_int("SHAPE_CONTEXT_INTENSITY_TYPE")
            self.plot_path = config.get_str("PLOT_PATH")


    def count_points_in_bin(self, center_pt, points):
        ''''
            Counts the number of points present in each bin of Circles X Sector bins
        '''
        features =[]
        entire_radius = self.radii[self.circle_count - 1]
        center = np.array([center_pt])
        distance_unit = entire_radius/(self.circle_count)
        angular_unit = 360.0/(self.sectors_count)
        feature_length= self.circle_count*self.sectors_count
        hist = np.zeros((self.sectors_count, self.circle_count), np.float64)
        # if there is only no shape context circle, then there is no feature set
        if (entire_radius == 0):
            return hist.reshape((feature_length,)).tolist()

        # find the ratio of distance of every point from center to the distance_unit
        # compute Filter of points that are with-in the radius
        distance_all_points = distance.cdist(center, points)
        points_with_filter = distance_all_points <= entire_radius
        #ignore points out-side the outer most circle
        dist_reqd_points = distance_all_points[points_with_filter]
        if dist_reqd_points.size == 0:
            return hist.reshape((feature_length,)).tolist()

        # clip to the count of circular bins, to clip points that are in the circle
        #ratio of distance to distance unit gives which circular bin the point falls into
        circle_bins = np.clip(np.floor(dist_reqd_points/distance_unit), 0, self.circle_count-1)

        # filter the points to be used basd on filter computed w.r,t distance
        points = points[points_with_filter.flatten()]
        X, Y = np.transpose(points)
        # find the angle formed by points with center, using tan2
        # get the angular bin based on the ratio formed with respect to angular unit
        # take modulus to get values in range [0, 2*pi]
        angle_bins =np.degrees((np.arctan2(Y-center_pt[1],X-center_pt[0]))%(math.pi*2))/angular_unit
        angle_bins = np.clip(np.floor(angle_bins),0, self.sectors_count-1)

        #find the count of points in each bin based on circular bin and angular bin
        bins_flatten = (circle_bins + angle_bins*self.circle_count).astype(int)
        count_of_points  = np.bincount(bins_flatten, minlength=feature_length)
        #normalize
        sum = count_of_points.sum()
        if sum !=0:
            count_of_points = count_of_points/ (sum)
        return count_of_points.reshape((feature_length,)).tolist()

    def get_shape_context_by_count(self, center, symbol_points, other_points):
        '''
            for the given symbol points and neighboring points compute the shape context features by counting
        '''
        features = []
        if (self.required_features != "CONTEXT_ONLY"):
            features += self.count_points_in_bin(center, symbol_points)
        if (self.required_features != "SYMBOL_ONLY") and other_points is not None:
            features += self.count_points_in_bin(center, other_points)
        return features

    def get_scc_base_radius(self, center, bounding_box, symbol_points, other_points):
        '''
           find the outer most circle radius based on the type of radius method
        '''
        if self.scc_radius_method == "BOUNDING_BOX_DIAGONAL":
            # get the bounding box diagonal of the symbol
            # s_minX, s_maxX, s_minY, s_maxY = current_strokes.original_box
            s_minX, s_maxX, s_minY, s_maxY = bounding_box
            return math.sqrt(math.pow(center[0] - s_maxX, 2) + math.pow(center[1] - s_maxY, 2))

        elif self.scc_radius_method == "NEAREST_NEIGHBOR":
            # get the minimum neighboring point distance as radius
            if len(other_points) < 1:
                return 0.0
            return distance.cdist([center],other_points).min()

        elif self.scc_radius_method == "FARTHEST_NEIGHBOR":
            # get the minimum neighboring point distance as radius
            if len(other_points) < 1:
                return 0.0
            return distance.cdist([center], other_points).max()

        elif self.scc_radius_method == "GIVEN_AMOUNT_CONTEXT":
            # get the minimum neighboring point distance as radius
            if len(other_points) < 1:
                return 0.0
            other_points_distance = distance.cdist([center],other_points)
            return np.nanpercentile(other_points_distance, q=self.context_fraction)

        elif self.scc_radius_method == "SYMBOL_FARTHEST_PT":
            return distance.cdist([center], symbol_points).max()

        else:
            raise Exception("Undefined SCC Parameter :Distance should be BOUNDING_BOX_DIAGONAL , SYMBOL_FARTHEST_PT, NEAREST_NEIGHBOR")

    def set_scc_radii_vector(self, entire_radius):
        '''
            based on the circle type and from the outer most radius sets the radii vector
        '''
        if (self.circle_type == "UNIFORM"):
            self.radii = np.linspace(start=entire_radius/self.circle_count, stop=entire_radius, num=self.circle_count, endpoint=True, )
        elif (self.circle_type == "LOG_POLAR"):
            #the inner most circle's radii
            least_radius=entire_radius/math.pow(2,self.circle_count-1)
            self.radii = np.logspace(start=math.log(least_radius,2), stop=math.log(entire_radius ,2), base=2, num=self.circle_count, endpoint=True, )
        else:
            raise Exception("Undefined SCC Parameter : CIRCLE TYPE NOT WELL DEFINED : UNIFORM / LOG-POLAR")

    def getBoundsBox(self, symbols):
        min_x = min([sym.minX for sym in symbols])
        min_y = min([sym.minY for sym in symbols])
        max_x = max([sym.maxX for sym in symbols])
        max_y = max([sym.maxY for sym in symbols])
        centerX = (min_x + max_x) / 2
        centerY = (min_y + max_y) / 2
        width = max_x-min_x
        height = max_y-min_y
        w_mod = self.config.get_float("BOUNDS_WIDTH_MODIFIER", 1.0)
        h_mod = self.config.get_float("BOUNDS_HEIGHT_MODIFIER", 1.0)
        min_x = centerX - (width*w_mod)/2
        max_x = centerX + (width*w_mod)/2
        min_y = centerY - (height*h_mod)/2
        max_y = centerY + (height*h_mod)/2
        return min_x, min_y, max_x, max_y

    def find_parzen_sample_points(self, center):
        # Find the co-ordinates at which the sample points are located for each of the bin
        # these are the references around which parzen features are computed

        sample_points = np.zeros((self.circle_count*self.sectors_count,2), np.float64)
        angle_unit = (2 * math.pi) / self.sectors_count

        for i in range(self.sectors_count):
            for j in range(self.circle_count):
                #find the polar co-ordinates of the sample points
                if (self.samples_type == "BIN_CORNERS"):
                    angle = (i * angle_unit)
                    sample_radius= self.radii[j]
                elif (self.samples_type == "BIN_CENTER"):
                    prev_circle_radius = self.radii[j - 1] if j != 0 else 0
                    current_circle_radius = self.radii[j]
                    #the midpoint of radii range the angular range
                    sample_radius = (prev_circle_radius+current_circle_radius)/2.0
                    angle = angle_unit * i + (angle_unit) / 2.0
                else:
                    raise Exception("Error in sample points specification : Center or Bin Corner")

                # convert them into cartesian co-ordinates
                delta_x = sample_radius * math.cos(angle)
                delta_y = sample_radius * math.sin(angle)
                #re align from the center
                x_coordinate = center[0] + delta_x
                y_coordinate = center[1] + delta_y
                sample_points[i*self.circle_count+j] = (x_coordinate, y_coordinate)
        return sample_points

    def get_shape_context_by_parzen(self, center, symbol_points, other_points):
        '''

        :param center: center of the shape context circles
        :param symbol_points: points in the symbol
        :param other_points: other points in the expression
        :return: shape context vector based on the required features
        '''
        self.sample_points = self.find_parzen_sample_points(center)
        variance = (self.radii[self.circle_count - 1] * self.standard_dev_factor) ** 2
        symbol_SCF=None
        neighbor_SCF = None
        if symbol_points is None:
            return []
        features =[]

        if (self.required_features != "CONTEXT_ONLY"):
            # Find the shape-context features of the current Symbol
            symbol_SCF = MathOps.get_parzen_density_vector(self.sample_points, np.array(symbol_points), variance)

        # Find the shape-context features of the neighboring points of the current symbol if context is needed
        if (self.required_features != "SYMBOL_ONLY") and other_points is not None:
            neighbor_SCF = MathOps.get_parzen_density_vector(self.sample_points, np.array(other_points), variance)

        #if context features - requires and empty neighbor list passed - return nothing
        #or append the values in single feature vector or return the one with values
        if symbol_SCF is not None and neighbor_SCF is not None:
            features += np.concatenate((symbol_SCF, neighbor_SCF), axis=0).tolist()
        elif symbol_SCF is not None or neighbor_SCF is not None:
            features += symbol_SCF.tolist() if neighbor_SCF is None else neighbor_SCF.tolist()

        # Concatenate the symbol features and Neighbor features
        return features

    def get_shape_context_by_inverse_distance(self, center, entire_radius, symbol_points):
        features = []

        #if there are no symbol_points return empty feature
        #This use-case makes the computation common for parsing & classification (where child-symbol points are None)
        if symbol_points == []:
            return features

        countPointOutside = 0
        # circle_count + 1 -> an extra row to represent the center of the circle
        hist = np.zeros((self.sectors_count, self.circle_count + 1), np.float64)

        radial_distance = entire_radius / (self.circle_count)
        angular_distance = 360.0 / (self.sectors_count)
        # Vectors for radial distances and angular distances
        radii_vector = []
        for i in range(self.circle_count + 1):
            radii_vector.append(radial_distance * (i))
        angles_vector = []
        for i in range(self.sectors_count):
            angles_vector.append(angular_distance * (i))
        corners = []
        # append the center
        corners.append((center[0], center[1]))
        for radius in radii_vector[1:]:
            for angle in angles_vector:
                x = radius * math.cos(math.radians(angle))
                y = radius * math.sin(math.radians(angle))
                corners.append((x, y))
        # For each point calculate the features
        for x, y in symbol_points:
            point = (x, y)
            dist_from_center = distance.cdist([center], [point])

            # the neighboring points that are out-side the shape context circle don't consider
            if dist_from_center > entire_radius:
                countPointOutside += 1
                continue

            if dist_from_center == 0:
                hist[0][0] += 1
                continue

            angle_at_center = math.atan2((y - center[1]), (x - center[0]))
            angle_at_center = math.degrees(angle_at_center)
            # normalizing the angle with 360 degrees as atan2 returns angle between -pi and pi
            if angle_at_center < 0:
                angle_at_center += 360
            # get all the bins for the point
            min_r = math.floor(dist_from_center / radial_distance)
            min_a = math.floor(angle_at_center / angular_distance) % self.sectors_count
            # If point is on the outermost circle
            if min_r == self.circle_count:
                min_r -= 1
            max_r = min_r + 1
            max_a = (min_a + 1) % self.sectors_count

            # Calculating the coordinates for all the corner points
            cornerPts = []
            i = (max_r - 1) * (self.sectors_count) + (min_a) + 1
            if i % self.sectors_count == 0:
                j = i - 5
            else:
                j = i + 1

            cornerPts.append(corners[i])
            cornerPts.append(corners[j])
            # avoid calculating the fourth point, in case of the innermost circle
            numberOfCornersUsed = 4
            if min_r != 0:
                cornerPts.append(corners[i - self.sectors_count])
                cornerPts.append(corners[j - self.sectors_count])
            else:
                numberOfCornersUsed = 3
                cornerPts.append((center[0], center[1]))

            # Used to store the inverse of the distance, membership weight for the corner point
            distWeight = [0.0] * numberOfCornersUsed
            for idx, corner in enumerate(cornerPts):
                tmpPt = np.array(point)
                tmpCr = np.array(corner)
                dist = distance.cdist([tmpPt], [tmpCr])
                if dist == 0:
                    distWeight = [0.0] * numberOfCornersUsed
                    distWeight[idx] = 1.0
                    break
                else:
                    distWeight[idx] = (1 / dist)

            N = sum(distWeight)
            distWeight = [x/N for x in distWeight]

            hist[min_a][max_r] += distWeight[0]
            hist[max_a][max_r] += distWeight[1]
            hist[min_a][min_r] += distWeight[2]
            # avoid calculating the fourth point, in case of the innermost circle
            if min_r != 0:
                hist[max_a][min_r] += distWeight[3]

        if countPointOutside > 0:
            print(countPointOutside,' lie outside the shape context circle')

        # Normalize the histogram
        normalization_factor = hist.sum()
        if normalization_factor > 0.0:
            hist /= normalization_factor
        # Separate the center from the circles and add it to feature list
        center_row = hist[:, 0]
        center_feature = sum(center_row)
        features.append(center_feature)
        # Add rest of the corner points to the feature list
        rest_hist = hist[:, 1:]
        features += rest_hist.reshape(((self.circle_count) * self.sectors_count,)).tolist()
        # return the features
        return features

    def get_shape_context_features(self, symbol, child_symbol=None, expression=None, use_expression=False, bound=False):
        '''
        :param symbol: symbol for which scc has to be extracted
        :param expression: expression with respect to which scc has to be extracted
        :return: scc features
        '''

        features = []
        # define center of the concentric circles from bounding box of symbol
        if child_symbol is None:
            s_minX, s_maxX, s_minY, s_maxY = bounding_box = symbol.minX, symbol.maxX, symbol.minY, symbol.maxY
            s_centerX = s_minX + (s_maxX - s_minX) / 2.0
            s_centerY = s_minY + (s_maxY - s_minY) / 2.0
            center = [s_centerX, s_centerY]
            symbol_points = expression.getSymbolPoints(symbol.getId())
            neighbor_points = expression.getContextPoints(symbol)
        else:
            s_minX, s_maxX, s_minY, s_maxY = symbol.minX, symbol.maxX, symbol.minY, symbol.maxY
            cs_minX, cs_maxX, cs_minY, cs_maxY = child_symbol.minX, child_symbol.maxX, child_symbol.minY, child_symbol.maxY

            # update the rectangular bounding box to be the common bounding box of two symbols, by extending to common symbol parameters
            s_minX, s_maxX, s_minY, s_maxY = min(s_minX, cs_minX), max(s_maxX, cs_maxX),min(s_minY, cs_minY), max(s_maxY, cs_maxY)
            #find the bounding box and the center
            bounding_box = s_minX, s_maxX, s_minY, s_maxY
            s_centerX = s_minX + (s_maxX - s_minX) / 2.0
            s_centerY = s_minY + (s_maxY - s_minY) / 2.0
            center = [s_centerX, s_centerY]
            #append the points of symbol and append, specify axis so that every item is (x,y) pair
            parent_points = expression.getSymbolPoints(symbol.getId())
            child_points = expression.getSymbolPoints(child_symbol.getId())
            symbol_points = np.append(parent_points, child_points, axis=0)
            neighbor_points = expression.getContextPoints(symbol, child_symbol)


        # get the required parameters from config. In-case of context, get the radius method, required-features(context, symbol etc)
        if use_expression:
            circles = self.config.get("EXPRESSION_SHAPE_CONTEXT_CIRCLES")
            scc_radius_method_list = self.config.get("EXPRESSION_SHAPE_CONTEXT_RADIUS_METHOD")
            radius_restriction_factor_list = self.config.get("EXPRESSION_SHAPE_CONTEXT_RADIUS_FACTOR")
            required_features_list = self.config.get("EXPRESSION_SHAPE_CONTEXT_USE_GLOBAL_CONTEXT")

            max_radii_factor = max(radius_restriction_factor_list)

        else:
            circles = self.config.get("SYMBOL_SHAPE_CONTEXT_CIRCLES")
            #max_radii_factor = 1

        for idx,circle in enumerate(circles):
            current_config_features =[]
            if use_expression:
                self.scc_radius_method = scc_radius_method_list[idx]
                self.radius_restriction_factor = radius_restriction_factor_list[idx]
                self.required_features = required_features_list[idx]

            if circle[3] == "PARZEN":
                circle_count, sector_count, circle_type, extraction_type, std_factor, samples_type, normalization_type = circle

                self.standard_dev_factor = std_factor
                #this is to update the standard-deviation values during grid search
                if self.standard_dev_factor_override is not None:
                    self.standard_dev_factor = self.standard_dev_factor_override

                self.samples_type = samples_type
                self.parzen_normalization_method = normalization_type
            else:
                circle_count, sector_count, circle_type, extraction_type = circle

            # this is to update the circles and sectors values during grid search
            self.circle_count = circle_count
            if self.circle_count_override:
                self.circle_count = self.circle_count_override

            self.sectors_count = sector_count
            if self.sectors_count_override:
                self.sectors_count = self.sectors_count_override

            self.circle_type = circle_type
            self.extraction_type = extraction_type

            #get the distance unit based on radius method and set the radii vector
            distance_unit = self.get_scc_base_radius(center, bounding_box, symbol_points, neighbor_points)
            self.set_scc_radii_vector(self.radius_restriction_factor * distance_unit)

            # Remove some of the Neighbor points from context
            if (self.required_features != "SYMBOL_ONLY") and neighbor_points is not None:
                if bound:
                    # Remove Neighbor points beyond the bounds box
                    if child_symbol is not None:
                        symbols = [symbol, child_symbol]
                    else:
                        symbols = [symbol]
                    min_x, min_y, max_x, max_y = self.getBoundsBox(symbols)
                    neighbor_points = np.array([point for point in neighbor_points if min_x <= point[0] <= max_x and min_y <= point[1] <= max_y])
                else:
                    # Remove Neighbor points beyond the farthest radius
                    max_radius = max_radii_factor * distance_unit
                    inside_indices = distance.cdist(np.array([center]), neighbor_points) <= max_radius
                    neighbor_points = np.array([neighbor_points[i] for i in range(neighbor_points.shape[0]) if inside_indices[0][i]])

                if neighbor_points.shape[0] == 0:
                    neighbor_points = np.array([]).reshape([0,2])

            # extract features by count, parzen or inverse distance
            if (self.extraction_type == "INVERSE_DISTANCE"):
                current_config_features = self.get_shape_context_by_inverse_distance(center, (self.radius_restriction_factor * distance_unit), symbol_points)
            elif (self.extraction_type == "PARZEN"):
                if child_symbol is None:
                    current_config_features += self.get_shape_context_by_parzen(center, symbol_points, neighbor_points)
                else:
                    current_config_features += self.get_shape_context_by_parzen(center, parent_points, neighbor_points)
                    current_config_features += self.get_shape_context_by_parzen(center, child_points, None)
            elif (self.extraction_type == "COUNT_POINTS"):
                if child_symbol is None:
                    current_config_features += self.get_shape_context_by_count(center, symbol_points, neighbor_points)
                else:
                    current_config_features += self.get_shape_context_by_count(center, parent_points, neighbor_points)
                    current_config_features += self.get_shape_context_by_count(center, child_points, None)
            else:
                raise Exception("METHOD OF EXTRACTING FEATURES should be either: INVERSE_DISTANCE, PARZEN, COUNT_POINTS")

            '''
            Visualization of the features if the extraction works on debug mode
            '''
            if self.debug_mode == True:

                # find sample points on other feature computation methods
                single_fea_len = self.circle_count * self.sectors_count

                self.plot_path = self.plot_path +"//"+ symbol.id
                #Create the path for visuals to get generated
                if not os.path.exists(self.plot_path):
                    os.makedirs(self.plot_path)

                if child_symbol is None:
                    file_prefix = str(symbol.id) + "_" + str(idx)
                else:
                    file_prefix = str(symbol.id) + "_" + str(child_symbol.id) + "_" + str(idx)


                '''
                    1.plot of symbols and expression in the shape context circles along with sample points
                    2. Colored bins- intensity of color represents the corresponding value of the bin in frature vector
                '''
                if self.extraction_type == "INVERSE_DISTANCE":
                    #Inverse distance is experimented only for symbol points and not for context points.
                    self.plot_sample_point_density(file_prefix, center, symbol_points, current_config_features)
                else:
                    #For both parzen and count based estimates, plot the symbol points and the bin visuals
                    if (self.required_features == "CONTEXT_AND_SYMBOL"):
                        self.plot_sample_point_density(file_prefix, center, parent_points, child_points, neighbor_points)
                        self.visualize_bin_density(file_prefix + "_Child", parent_points,
                                                   current_config_features[:single_fea_len], cmap_val=cm.Reds)
                        self.visualize_bin_density(file_prefix + "_Neighbor", child_points,
                                                   current_config_features[single_fea_len:2*single_fea_len], cmap_val=cm.Blues)
                        self.visualize_bin_density(file_prefix + "_Parent", neighbor_points,
                                         current_config_features[2*single_fea_len:], cmap_val=cm.Greens)
                    elif (self.required_features == "SYMBOL_ONLY"):
                        self.plot_sample_point_density(file_prefix, center, parent_points, child_points,
                                                       neighbor_points)
                        self.visualize_bin_density(file_prefix, symbol_points, current_config_features[:single_fea_len], cmap_val=cm.Greens)
                    else:
                        self.plot_sample_point_density(file_prefix, center, parent_points, child_points,
                                                       neighbor_points)
                        self.visualize_bin_density(file_prefix + "_Neighbor", neighbor_points,
                                                   current_config_features[:single_fea_len], cmap_val=cm.Blues)

                '''
                Plot the special visuals for parzen estimation
                3. Contributors visualization - For a given bin, intensity of contribution from each of the symbol or other point. 
                '''
                # if (self.extraction_type == "PARZEN"):
                #     #Include the tag 'contributors' in directory path
                #     directory_prefix = "contributors//"
                #     if not os.path.exists(self.plot_path+directory_prefix):
                #         os.makedirs(self.plot_path+directory_prefix)
                #     if (self.required_features != "CONTEXT_ONLY"):
                #         self.visualize_bin_density_contributors(directory_prefix+file_prefix, symbol_points, center)
                #     if (self.required_features != "SYMBOL_ONLY"):
                #         self.visualize_bin_density_contributors(directory_prefix+file_prefix+"_NEIGHBOR", neighbor_points, center)


            features+=current_config_features


        return features

    ## PLOTTING AND DRAWING METHODS START HERE ##

    def plotSectors(self, center, radius):
        # draw the sector lines in shape context circles for reference
        angle_unit = (2 * math.pi) / self.sectors_count
        for i in range(0, self.sectors_count):
            angle = angle_unit * i
            x = [center[0], center[0] + radius *math.cos(angle)]
            y = [center[1], center[1] + radius * math.sin(angle)]
            plt.plot(x, y, 'k-')

    def plot_sample_point_density(self, filename, center, parent_points, child_points, neighbor_points):
        '''

            Plot the points in the symbol and the neighboring points
            Scatter the sample points considered colored equivalent to it's density
        '''
        plt.clf()
        fig = plt.gcf()
        # draw circles
        shape_context_radius= self.radii[self.circle_count-1]
        self.draw_shape_context_circles(center)
        # draw Sectors
        self.plotSectors(center, shape_context_radius)
        # plot current points
        fig = plt.gcf()
        if (self.extraction_type == "PARZEN"):
            varRadius = shape_context_radius * self.standard_dev_factor
        for point in parent_points:
            plt.plot(point[0], point[1], 'r.')

        for point in child_points:
                plt.plot(point[0], point[1], 'g.')

        for point in neighbor_points:
            plt.plot(point[0], point[1], 'b.')
            # draw the parzen circles around the symbol points
            '''if (self.extraction_type == "PARZEN"):
                parzenCircle = plt.Circle((point[0], point[1]), varRadius, fill=False, color='y')
                fig.gca().add_artist(parzenCircle)'''
        plt.gca().invert_yaxis()
        # plot the sample points

        '''if (self.extraction_type == "PARZEN"):
            plt.scatter(self.sample_points[:,:1], self.sample_points[:,1:], c=current_density, s=200, marker="o", cmap=cm.Reds)
            plt.colorbar()'''

        plt.axis('scaled')
        plt.axis('off')
        plt.savefig(self.plot_path + filename + "_EXPRESSION.png")
        plt.clf()

    def draw_shape_context_circles(self, center):
        # plot the shape context circles for reference
        fig = plt.gcf()
        for i in range(self.circle_count):
            circleFig_i = plt.Circle((center[0], center[1]), self.radii[i], fill=False, color='k')
            fig.gca().add_artist(circleFig_i)

    def visualize_bin_density_contributors(self, file_name, current_points, center):
        '''
         Plot the contribution to every bin from every point in the symbol
        '''
        plt.clf()
        self.draw_shape_context_circles(center)

        X_Bins =[]
        Y_Bins=[]
        for pt_i in current_points:
            # find intensity pt_i contributes the current sample_point
            pt_i_list = []
            pt_i_list.append(pt_i)
            X_Bins.append(pt_i[0])
            Y_Bins.append(pt_i[1])

        variance = (self.radii[self.circle_count - 1] * self.standard_dev_factor) ** 2
        for i,sample_point in enumerate(self.sample_points):
                intensity = MathOps.get_parzen_density_vector(current_points,np.array([sample_point]), variance, False)
                self.draw_shape_context_circles(center)
                self.plotSectors(center, self.radii[self.circle_count-1])
                plt.scatter(sample_point[0], sample_point[1], s=500, marker="o", cmap=cm.Blues)
                plt.scatter(X_Bins, Y_Bins, c=intensity, s=50, marker="o", cmap=cm.Reds)
                plt.gca().invert_yaxis()
                plt.colorbar()
                plt.savefig(self.plot_path+ file_name + str(i)  + "_CONTRIBUTORS.png")
                plt.clf()

    def visualize_bin_density(self, filename, current_points, density, cmap_val=cm.Reds):
        '''
            The visual plot of the shape context feature vector in the circle
            The color of each bin of the
        '''
        theta = np.repeat((np.linspace(0.0, 2 * np.pi, self.sectors_count, endpoint=False)), self.circle_count)
        radii = []
        #plot the current points
        plt.clf()
        for point in current_points:
            plt.plot(point[0], point[1], 'k.')

        for i in range(self.sectors_count):
            for j in range(self.circle_count):
                radii.append(self.radii[self.circle_count-1-j])

        width = 2 * np.pi / self.sectors_count
        ax = plt.subplot(111, projection='polar')
        # remove grids and axis
        ax.grid(False)
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        ax.spines['polar'].set_visible(False)

        bars = ax.bar(theta, radii, width=width, bottom=0.0)
        # Use custom colors and opacity
        rotated_density = np.zeros(self.sectors_count*self.circle_count)

        for i in range(self.sectors_count):
            for j in range(self.circle_count):
                sector_index = (self.sectors_count - i) % self.sectors_count
                sector_index = (sector_index - 1) % self.sectors_count
                rotated_density[sector_index * self.circle_count + j] = density[
                    i * self.circle_count + (self.circle_count - 1 - j)]

        # normalize value in[0,1]
        maxd = max(rotated_density)
        if (self.intensity_type == 0):
            maxd = max(rotated_density)
            rotated_density = rotated_density/maxd

        for r, bar, color_code in zip(radii, bars, rotated_density):
            bar.set_facecolor(cmap_val(color_code))
            bar.set_alpha(1.0)
            bar.set_edgecolor('black')

        max_r = max(radii)
        for angle in set(theta):
            plt.annotate(math.ceil(math.degrees(angle)), xy=(angle, max_r), xytext=(angle, 1.1*max_r),
                         horizontalalignment='center', verticalalignment='center')
        xlist = []
        ylist = []
        self.samples_type = "BIN_CENTER"
        sample_points = self.find_parzen_sample_points([0.0,0.0])
        for x,y in sample_points:
            xlist.append(x)
            ylist.append(y)


        plt.scatter(xlist, ylist, c=rotated_density, s=0, marker="o", cmap=cmap_val)
        plt.colorbar()


        plt.axis('scaled')
        plt.savefig(self.plot_path + filename + "COLORED_BINS.png")
        plt.clf()

    def visualize_bin_density_at_corners(self, filename, density):
        center = [0.0, 0.0]
        plt.clf()
        # draw circles
        shape_context_radius = self.radii[self.circle_count - 1]
        self.draw_shape_context_circles(center)
        # draw Sectors
        self.plotSectors(center, shape_context_radius)
        plt.gca().invert_yaxis()

        theta = np.repeat((np.linspace(0.0, 2 * np.pi, self.sectors_count, endpoint=False)), 1)

        print(theta)
        for angle in set(theta):
            x = shape_context_radius * math.cos(angle)
            y = shape_context_radius * math.sin(angle)
            plt.annotate(math.ceil(math.degrees(angle)), xy=(x,-y), xytext=(1.25*x,1.25*-y), horizontalalignment='center', verticalalignment='center')

        xlist = []
        ylist = []
        xlist.append(0.0)
        ylist.append(0.0)
        self.samples_type = "BIN_CORNERS"
        sample_points = self.find_parzen_sample_points([0.0, 0.0])
        for x, y in sample_points:
            xlist.append(x)
            ylist.append(y)

        plt.scatter(xlist, ylist, c=density, s=300, marker="o", cmap=cm.Reds)
        plt.colorbar()

        plt.axis('scaled')
        plt.axis('off')
        plt.savefig(self.plot_path + filename + "COLORED_BINS.png")
        plt.clf()
