
import random
import numpy as np
from recognizer.util.math_ops import MathOps
from recognizer.visualization.SVG_generator import SVGGenerator

class TemporalFeatureExtractor:

    def __init__(self, config):
        self.config = config

    def getFeatures(self, symbol, trace_order=None):
        features = []

        n_points = self.config.get_int("TEMPORAL_FEATURES_SAMPLE_SIZE", -1)

        if n_points < 1:
            raise Exception("Invalid number of Points to use for temporal features")

        # temporal
        if self.config.get_bool("TEMPORAL_FEATURES_NUM_TRACES"):
            features.append(len(symbol.traces))

        trace_length, connect_length, point_sample = TemporalFeatureExtractor.sample_symbol_points(symbol, n_points, trace_order)

        if self.config.get_bool("TEMPORAL_FEATURES_TRACE_LENGTH"):
            features.append(trace_length)
        if self.config.get_bool("TEMPORAL_FEATURES_CONNECT_LENGTH"):
            features.append(connect_length)
        if self.config.get_bool("TEMPORAL_FEATURES_TOTAL_LENGTH"):
            features.append(trace_length + connect_length)

        for (px, py), pen_up in point_sample:
            if self.config.get_bool("TEMPORAL_FEATURES_SAMPLE_USE_X"):
                features.append(px)
            if self.config.get_bool("TEMPORAL_FEATURES_SAMPLE_USE_Y"):
                features.append(py)
            if self.config.get_bool("TEMPORAL_FEATURES_SAMPLE_USE_UP"):
                features.append(pen_up)

        return features


    def getRelationFeatures(self, expression, parent, child):
        features = []

        if self.config.get_bool("AVERAGE_TIME_GAP"):
            features.append(TemporalFeatureExtractor.averageTimeGap(parent, child))

        if self.config.get_bool("MIN_TIME_GAP"):
            features.append(TemporalFeatureExtractor.minTimeGap(parent, child))

        if self.config.get_bool("MAX_TIME_GAP"):
            features.append(TemporalFeatureExtractor.maxTimeGap(parent, child))

        return features

    @staticmethod
    def averageTimeGap(parent, child):
        return np.average(np.array(child.trace_list).astype(int)) - np.average(np.array(parent.trace_list).astype(int))

    @staticmethod
    def minTimeGap(parent, child):
        return np.min(np.abs(np.concatenate([np.array(child.trace_list).astype(int) - pid for pid in np.array(parent.trace_list).astype(int)])))

    @staticmethod
    def maxTimeGap(parent, child):
        return np.max(np.abs(np.concatenate([np.array(child.trace_list).astype(int) - pid for pid in np.array(parent.trace_list).astype(int)])))


    @staticmethod
    def sample_symbol_points(symbol, n_points, trace_order=None):
        n_traces = len(symbol.traces)
        if trace_order is None:
            trace_order = [idx for idx in range(n_traces)]

        # compute total length including pen-up traces
        # also create single array of points
        trace_length = 0.0
        connect_length = 0.0
        all_points = []
        is_pen_up = []
        for idx in range(n_traces):
            curr_trace = symbol.traces[trace_order[idx]]

            all_points += list(curr_trace.points)
            is_pen_up += [0 for idx in range(len(curr_trace.points) - 1)] + [1]

            trace_length += curr_trace.getTotalLength()
            if idx > 0:
                # add the length connecting to the previous trace
                prev_trace = symbol.traces[trace_order[idx - 1]]

                x1, y1 = prev_trace.points[-1]
                x2, y2 = curr_trace.points[0]

                connect_length += MathOps.euclideanDistance(x1, y1, x2, y2)

        total_length = trace_length + connect_length

        # format ((x, y), on_trace)
        # if on_trace = False, then the point is in a pen-up trace
        # first
        point_sample = [(all_points[0], 0)]

        if n_points >= 2 and total_length > 0.0:

            # elements in between ...
            sample_dist = total_length / (n_points - 1)

            current_point = 0
            c_x, c_y = all_points[current_point]
            n_x, n_y = all_points[current_point + 1]
            start_length = 0.0
            finish_length = MathOps.euclideanDistance(c_x, c_y, n_x, n_y)

            next_sample = 0.0
            for idx in range(1, n_points - 1):
                next_sample += sample_dist

                while next_sample >= finish_length:
                    # move to the next point
                    current_point += 1
                    c_x, c_y = n_x, n_y
                    n_x, n_y = all_points[current_point + 1]

                    # update distance
                    start_length = finish_length
                    finish_length += MathOps.euclideanDistance(c_x, c_y, n_x, n_y)

                # use linear interpolation
                w1 = (next_sample - start_length) / (finish_length - start_length)
                #print((idx, next_sample, start_length, finish_length, current_point, 1.0 - w1, w1), flush=True)

                assert 0.0 <= w1 < 1.0

                sample_x = n_x * w1 + c_x * (1.0 - w1)
                sample_y = n_y * w1 + c_y * (1.0 - w1)
                sample_up = is_pen_up[current_point]

                point_sample.append(((sample_x, sample_y), sample_up))

            # add last
            point_sample.append((all_points[-1], 0))
        else:
            # only one point in this symbol, fill with only point
            for idx in range(1, n_points):
                # add last to fill the array ...
                point_sample.append((all_points[-1], 0))

        return trace_length, connect_length, point_sample



    def get_relative_features(self,parent_symbol, child_symbol):
        return[int(parent_symbol.getId()) - int(child_symbol.getId())]


