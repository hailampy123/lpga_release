import pickle

class CodeBook:
    def __init__(self, codes):
        self.codes = codes

    def save_to_file(self, configuration):
        out_filename = configuration.data["CODE_BOOK_DIR"] + "/" + configuration.data["CODE_BOOK_NAME"] + ".dat"
        out_file = open(out_filename, "wb")
        pickle.dump(self, out_file, pickle.HIGHEST_PROTOCOL)
        out_file.close()

    @staticmethod
    def load_from_file(configuration):
        input_filename = configuration.data["CODE_BOOK_DIR"] + "/" + configuration.data["CODE_BOOK_NAME"] + ".dat"
        in_file = open(input_filename, "rb")
        code_book = pickle.load(in_file)
        in_file.close()

        return code_book